########################################################################
#
#                           GettingStarted.eww
#
# $Revision: 16 $
#
########################################################################

DESCRIPTION
===========
  This example project shows how to use the IAR Embedded Workbench for ARM
 to develop code for the Logic/Freescale iMX28 XPCIMX28LEVK board. It shows
 basic use of I/O, system initialization (PLL, POWER, EMI, MMU, ICOLL)
 and timer.


COMPATIBILITY
=============

   The example project is compatible with, on Freescale XPCIMX28LEVK board
   By default, the project is configured to use the J-Link JTAG interface.

CONFIGURATION
=============

GETTING STARTED
===============

  1) Start the IAR Embedded Workbench for ARM.

  2) Select File->Open->Workspace...
     Open the following workspace:

     <installation-root>\arm\examples\Freescale\
     MX28\XPCIMX28LEVK\GettingStarted\GettingStarted.eww
  
  3) Run the program.
