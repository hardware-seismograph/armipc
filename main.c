/*************************************************************************
 *
 *    Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2011
 *
 *    File name   : main.c
 *    Description : main module
 *
 *    History :
 *    1. Date        : June 28, 2011
 *       Author      : Stanimir Bonev
 *       Description : Create
 *
 *  This example project shows how to use the IAR Embedded Workbench for ARM
 * to develop code for the Logic/Freescale iMX28 XPCIMX28LEVK board. It shows
 * basic use of I/O, system initialization (PLL, POWER, EMI, MMU, ICOLL)
 * and timer.
 *
 *    $Revision: 16 $
 *
 **************************************************************************/

#include "ipc/ipc.h"

void startIPC(void);

/*************************************************************************
 * Function Name: main
 * Parameters: none
 *
 * Return: none
 *
 * Description:
 *
 *************************************************************************/


int main(void)
{
#ifdef MMU_ENA
  // Init MMU
  CP15_Mmu(FALSE);            // Disable MMU
  // Privileged permissions  User permissions AP
  // Read-only               Read-only        0
  CP15_SysProt(FALSE);
  CP15_RomProt(TRUE);
  CP15_InitMmuTtb(TtSB,TtTB); // Build L1 and L2 Translation tables
  CP15_SetTtb(L1Table);       // Set base address of the L1 Translation table
  CP15_SetDomain( (DomainManager << 2*1) | (DomainClient << 0)); // Set domains
  CP15_Mmu(TRUE);             // Enable MMU
  CP15_Cache(TRUE);           // Enable ICache,DCache
#endif

  ICOLL_Init();

  /* Init LEDs' outputs */
  HW_PINCTRL_MUXSEL6_bit.BANK3_PIN04 = 3;
  HW_PINCTRL_DRIVE12_bit.BANK3_PIN04_V = 1; // 3.3V
  HW_PINCTRL_DRIVE12_bit.BANK3_PIN04_MA = 0; // 4mA
  /* Enable output */
  HW_PINCTRL_DOE3_SET = 1 << 4;
  /* LED3 off*/
  HW_PINCTRL_DOUT3_CLR = 1 << 4;
  HW_PINCTRL_MUXSEL6_bit.BANK3_PIN05 = 3;
  HW_PINCTRL_DRIVE12_bit.BANK3_PIN05_V = 1; // 3.3V
  HW_PINCTRL_DRIVE12_bit.BANK3_PIN05_MA = 0; // 4mA
  /* Enable output */
  HW_PINCTRL_DOE3_SET = 1 << 5;
  /* LED2 off*/
  HW_PINCTRL_DOUT3_CLR = 1 << 5;

  HW_PINCTRL_MUXSEL6_bit.BANK3_PIN00 = 0;
  HW_PINCTRL_MUXSEL6_bit.BANK3_PIN01 = 0;
  HW_PINCTRL_MUXSEL6_bit.BANK3_PIN02 = 0;

  __disable_interrupt();
// **************************************************************************************************************************************************************************

  while(1)
  {
    startIPC();
  }
}
