//#include "board.h"
//#include "ipc/ipc.h"

#define BAUD9600        9600
#define BAUD19200       19200

#define MODEM_PORT      0
#define BRIDGE_PORT     1

#define NUMBER_OF_AUARTS 4

typedef unsigned int REG32;

typedef enum
{
  IDLE,
  TRANSMITTING,
  RECEIVING
}PORT_STATE;


typedef struct MX_AUART_PORT
{
  PORT_STATE state;
  int recvCnt;
  uchar inBuffer[256];
  int xmitCnt;
  int xLength;
  uchar outBuffer[256];
  REG32 *membase;
  ushort cts;
  ushort errors;
}MX_AUART_PORT;


int sio_write(int portNumber, uchar *buffer, int length);
int sio_read(int portNumber, uchar *buffer, int length);
void sio_read_abort(int portNumber);
void startRecv(int poerNumber);
void putC(int portNumber, char c);
char *getS(int portNumber, int wait);
void putS(int portNumber, char *string);
int getC(int portNumber, int wait);
void setRTS(int portNumber, int state);
int getCTS(int portNumber);
void getBaseAddress(int portNumber);
void resetPort(int portNumber);
void serialOpen(int portNumber, int baudrate, int cts, int rts);
void writeReg(REG32 *regAddress, int offset, REG32 data);
REG32 readReg(REG32 *regAddress, int offset);
void serialFlush(int port);
void addSerial(int port, uchar *buffer, int length);
void startXmit(int port);
int serialInt(void);


