/*
 * Freescale STMP37XX/STMP378X Application UART driver
 *
 * Author: dmitry pervushin <dimka@embeddedalley.com>
 *
 * Copyright 2008-2010 Freescale Semiconductor, Inc.
 * Copyright 2008 Embedded Alley Solutions, Inc All Rights Reserved.
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include "ipc/ipc.h"

#define MXS_AUART_PORTS 5
#define ECHO_ON

#define AUART_CTRL0			0x00000000
#define AUART_CTRL0_SET			0x00000004
#define AUART_CTRL0_CLR			0x00000008
#define AUART_CTRL0_TOG			0x0000000c
#define AUART_CTRL1			0x00000010
#define AUART_CTRL1_SET			0x00000014
#define AUART_CTRL1_CLR			0x00000018
#define AUART_CTRL1_TOG			0x0000001c
#define AUART_CTRL2			0x00000020
#define AUART_CTRL2_SET			0x00000024
#define AUART_CTRL2_CLR			0x00000028
#define AUART_CTRL2_TOG			0x0000002c
#define AUART_LINECTRL			0x00000030
#define AUART_LINECTRL_SET		0x00000034
#define AUART_LINECTRL_CLR		0x00000038
#define AUART_LINECTRL_TOG		0x0000003c
#define AUART_LINECTRL2			0x00000040
#define AUART_LINECTRL2_SET		0x00000044
#define AUART_LINECTRL2_CLR		0x00000048
#define AUART_LINECTRL2_TOG		0x0000004c
#define AUART_INTR			0x00000050
#define AUART_INTR_SET			0x00000054
#define AUART_INTR_CLR			0x00000058
#define AUART_INTR_TOG			0x0000005c
#define AUART_DATA			0x00000060
#define AUART_STAT			0x00000070
#define AUART_DEBUG			0x00000080
#define AUART_VERSION			0x00000090
#define AUART_AUTOBAUD			0x000000a0

#define AUART_CTRL0_SFTRST			(1 << 31)
#define AUART_CTRL0_CLKGATE			(1 << 30)

#define AUART_CTRL2_CTSEN			(1 << 15)
#define AUART_CTRL2_RTSEN                       (1 << 14)
#define AUART_CTRL2_RTS				(1 << 11)
#define AUART_CTRL2_RXE				(1 << 9)
#define AUART_CTRL2_TXE				(1 << 8)
#define AUART_CTRL2_UARTEN			(1 << 0)
#define AUART_CTRL2_RXIFLSEL(v)                 (((v) & 0x03) << 20)
#define AUART_CTRL2_TXIFLSEL(v)                 (((v) & 0x03) << 16)

#define AUART_LINECTRL_BAUD_DIVINT_SHIFT	16
#define AUART_LINECTRL_BAUD_DIVINT_MASK		0xffff0000
#define AUART_LINECTRL_BAUD_DIVINT(v)		(((v) & 0xffff) << 16)
#define AUART_LINECTRL_BAUD_DIVFRAC_SHIFT	8
#define AUART_LINECTRL_BAUD_DIVFRAC_MASK	0x00003f00
#define AUART_LINECTRL_BAUD_DIVFRAC(v)		(((v) & 0x3f) << 8)
#define AUART_LINECTRL_WLEN_MASK		0x00000060
#define AUART_LINECTRL_WLEN(v)			(((v) & 0x3) << 5)
#define AUART_LINECTRL_FEN			(1 << 4)
#define AUART_LINECTRL_STP2			(1 << 3)
#define AUART_LINECTRL_EPS			(1 << 2)
#define AUART_LINECTRL_PEN			(1 << 1)
#define AUART_LINECTRL_BRK			(1 << 0)

#define AUART_INTR_RTIEN			(1 << 22)
#define AUART_INTR_TXIEN			(1 << 21)
#define AUART_INTR_RXIEN			(1 << 20)
#define AUART_INTR_CTSMIEN			(1 << 17)
#define AUART_INTR_RTIS				(1 << 6)
#define AUART_INTR_TXIS				(1 << 5)
#define AUART_INTR_RXIS				(1 << 4)
#define AUART_INTR_CTSMIS			(1 << 1)

#define AUART_STAT_BUSY				(1 << 29)
#define AUART_STAT_CTS				(1 << 28)
#define AUART_STAT_TXFE				(1 << 27)
#define AUART_STAT_TXFF				(1 << 25)
#define AUART_STAT_RXFE				(1 << 24)
#define AUART_STAT_OERR				(1 << 19)
#define AUART_STAT_BERR				(1 << 18)
#define AUART_STAT_PERR				(1 << 17)
#define AUART_STAT_FERR				(1 << 16)

//static struct uart_driver auart_driver;

MX_AUART_PORT port[NUMBER_OF_AUARTS];

static unsigned int testValue;

void writeReg(REG32 *regAddress, int offset, REG32 data)
{
  unsigned char *p = (unsigned char *)regAddress;
  p += offset;
  REG32 *dest = (REG32 *)p;
  *dest = data;
  testValue = *dest;
  return;
}

REG32 readReg(REG32 *regAddress, int offset)
{
 unsigned char *p = (unsigned char *)regAddress;
 p += offset;
 REG32 *source = (REG32 *)p;
 testValue = *source;
 return(*source);
}

REG32 memBaseAddress[5] =
{
  0x8006a000,
  0x8006c000,
  0x8006e000,
  0x80070000,
  0x80072000
};

// *************************************************************
// these functions are used to support the lwip library calls
// *************************************************************

int sio_write(int portNumber, uchar *buffer, int length)
{
	addSerial(portNumber, buffer, length);
	return(length);// If an error occurs then return a 0
}

int sio_read(int portNumber, uchar *buffer, int length)
{
 int c = 0;

	c = getC(portNumber, 1000);
	if(c == -1)
		return(0);
	else
		return(c);
}

void sio_read_abort(int portNumber)
{
	resetPort(portNumber);
	return;
}

// **********************************************
//   int putC(int portNumber, char c);
// **********************************************

void putC(int portNumber, char ch)
{
 MX_AUART_PORT *s = &port[portNumber];
 REG32 stat = -1;
 uchar c;
	addSerial(portNumber, &c, 1);
	startXmit(portNumber);
    return;
}


// **********************************************
// void putS(int portNumber, char *string);
// **********************************************

void putS(int portNumber, char *s)
{
int length = strlen(s) - 1;
  addSerial(portNumber, (uchar *)s, length);
  addSerial(portNumber, (uchar *)"\r", 1);
  startXmit(portNumber);
  return;
}

// **********************************************
//   int getC(int portNumber, int wait);
// **********************************************

int getC(int portNumber, int wait)
{
 TIMER waitTimer;
 MX_AUART_PORT *s = &port[portNumber];
 REG32 stat = -1;
 REG32 c;

    setTimer(&waitTimer, wait);         // Setup the wait timer

    while(stat & AUART_STAT_RXFE)
    {
      stat = readReg(s->membase, AUART_STAT);
      if(checkTimer(&waitTimer))        // See if a timeout occurred
        break;
    }
    if(!checkTimer(&waitTimer))
      c = readReg(s->membase, AUART_DATA);
    else
      c = -1;         // Signal timeout

#ifdef ECHO_ON
    if(c != -1)
    {
    	writeReg(s->membase, AUART_CTRL2_SET, AUART_CTRL2_TXE);
    	writeReg(s->membase, AUART_DATA, c);
    }
#endif
    return(c);
}

// **********************************************
//   int getS(int portNumber, int wait);
// **********************************************

static char temp[256];

char * getS(int portNumber, int wait)
{
 char c;
 char *p = temp;

    *p = '\0';
    do
    {
        c = getC(portNumber, 1000);
        if(c != 0xff)
          *p++ = c;
    }while((c != '\r') && (c != 0xff));

 return(temp);
}


// **********************************************
// void startRecv(int portNumber);
// **********************************************

void startRecv(int portNumber)
{
 MX_AUART_PORT *s = &port[portNumber];

    writeReg(s->membase, AUART_LINECTRL_CLR, AUART_LINECTRL_FEN);     // Disable FIFO
    writeReg(s->membase, AUART_CTRL2_SET, AUART_CTRL2_RXE);

    return;
}

// **********************************************
// setRTS(int port, int state);
// **********************************************

void setRTS(int portNumber, int state)
{
 REG32 ctrl2;
 MX_AUART_PORT *s = &port[portNumber];

    ctrl2 = AUART_CTRL2_RTS;
    if(state)           // Remember the state is reversed
      writeReg(s->membase, AUART_CTRL2_CLR, ctrl2);
    else
      writeReg(s->membase, AUART_CTRL2_SET, ctrl2);

    return;
}

// **********************************************
// Returns the state of CTS
// **********************************************

int getCTS(int portNumber)
{
 REG32 stat;
 MX_AUART_PORT *s = &port[portNumber];

      stat = readReg(s->membase, AUART_STAT);
      s->cts = (stat & AUART_STAT_CTS) ? TRUE : FALSE;

      return(s->cts);
}


// **********************************************
// Sets up the base address for the auart#
// **********************************************

void getBaseAddress(int portNumber)
{
  port[portNumber].membase = (unsigned int *)memBaseAddress[portNumber];
  return;
};

// ***********************************************
//   serialFlush(int portNumber);
// ***********************************************

void serialFlush(int portNumber)
{
  port[portNumber].xLength = port[portNumber].recvCnt = port[portNumber].xmitCnt = 0;
  port[portNumber].state = IDLE;
  memset(&port[portNumber].inBuffer, 0, sizeof(port[portNumber].inBuffer));
  memset(&port[portNumber].outBuffer, 0, sizeof(port[portNumber].outBuffer));
  return;
}

// *******************************************************
//      addSerial(int port, uchar *buffer, int length);
// *******************************************************

void addSerial(int portNumber, uchar *buff, int length)
{
 uchar *p = port[portNumber].outBuffer + port[portNumber].xLength;
  // Stop buffer overruns here
  if((port[portNumber].xLength + length) > sizeof(port[portNumber].outBuffer))
     return;
  memcpy(p, buff, length);
  port[portNumber].xLength += length;

  return;
}

// ****************************************
//      resetPort()
// ****************************************

void resetPort(int portNumber)
{
 int i;
 unsigned int reg;
   int shortDelay;

	writeReg(port[portNumber].membase, AUART_CTRL0_CLR, (unsigned int)AUART_CTRL0_SFTRST);

	for (i = 0; i < 10000; i++)
        {
		reg = readReg(port[portNumber].membase, AUART_CTRL0);
		if (!(reg & (unsigned int)AUART_CTRL0_SFTRST))
			break;
		for(shortDelay = 0; shortDelay < 100; shortDelay++)
                    ;
	}
	writeReg(port[portNumber].membase, AUART_CTRL0_CLR, AUART_CTRL0_CLKGATE);
        return;
}



// ******************************************************************
//      serialOpen(int portNmber, int baudrate, int cts, int rts);
//      This is the main configuration function for the serial port.
// ******************************************************************

void serialOpen(int portNumber, int baudrate, int cts, int rts)
{
 unsigned long bm, linectrl, ctrl2, div;

        getBaseAddress(portNumber);

        resetPort(portNumber);

 	writeReg(port[portNumber].membase, AUART_CTRL2_SET, AUART_CTRL2_UARTEN);

	/*
	 * Enable fifo so all four bytes of a DMA word are written to
	 * output (otherwise, only the LSB is written, ie. 1 in 4 bytes)
	 */

        ctrl2 = AUART_CTRL2_RXIFLSEL(3);
        ctrl2 |= AUART_CTRL2_TXIFLSEL(3);
        writeReg(port[portNumber].membase, AUART_CTRL2_CLR, ctrl2);

        // Read the current value of registers
        linectrl = 0;
        //        linectrl = readReg(port[portNumber].membase, AUART_LINECTRL);
	ctrl2 = readReg(port[portNumber].membase, AUART_CTRL2);

	// Enable fifo so all four bytes of a DMA word are written to
	// output (otherwise, only the LSB is written, ie. 1 in 4 bytes)	 */

	linectrl |= AUART_LINECTRL_FEN;     // Enable FIFO

	// byte size (8)
	bm = 3;

	linectrl |= AUART_LINECTRL_WLEN(bm);

	// set baud rate
	div = FOSC1 * 32 / baudrate;

	linectrl |= AUART_LINECTRL_BAUD_DIVFRAC(div & 0x3F);
	linectrl |= AUART_LINECTRL_BAUD_DIVINT(div >> 6);

        // Setup the recv/xmit FIFOs
        ctrl2 |= AUART_CTRL2_RXIFLSEL(0x01);
        ctrl2 |= AUART_CTRL2_TXIFLSEL(0x04);

        if(cts)
          ctrl2 |= AUART_CTRL2_CTSEN;
        if(rts)
          ctrl2 |= AUART_CTRL2_RTSEN;

        ctrl2 |= AUART_CTRL2_UARTEN;

	writeReg(port[portNumber].membase, AUART_LINECTRL, linectrl);
	writeReg(port[portNumber].membase, AUART_CTRL2, ctrl2);

        return;
}

// *****************************
//        startXmit
// *****************************

void startXmit(int portNumber)
{
 MX_AUART_PORT *s = &port[portNumber];
 TIMER xmitTimer;
 uchar *p = port[portNumber].outBuffer;
 int tempLength = port[portNumber].xLength;

            if(port[portNumber].xLength == 0)
              return;

            setTimer(&xmitTimer, 1000);

            writeReg(s->membase, AUART_LINECTRL_SET, AUART_LINECTRL_FEN);     // Enable FIFO

             // Enable transmitter
            writeReg(s->membase, AUART_CTRL2_SET, AUART_CTRL2_TXE);

            s->state = TRANSMITTING;

            while(!(readReg(s->membase, AUART_STAT) & AUART_STAT_TXFF))
            {
                port[portNumber].xmitCnt++;
		writeReg(s->membase, AUART_DATA, *p++);
                if(--tempLength)
                  continue;
                else
                  break;
            }

        if(portNumber == 0)
        {
          ICOLL_SetupIntr((IntrFunc_t)serialInt, 0, INT_AUART0, LEVEL3);
          /*Enable Interrupt in ICOLL*/
          ICOLL_EnableIntr(INT_AUART0);
        }else if(portNumber == 1)
        {
          ICOLL_SetupIntr((IntrFunc_t)serialInt, 0, INT_AUART1, LEVEL3);
          /*Enable Interrupt in ICOLL*/
          ICOLL_EnableIntr(INT_AUART1);
        }
//        writeReg(s->membase, AUART_INTR_SET, AUART_INTR_TXIEN);
	writeReg(port[portNumber].membase, AUART_INTR, (AUART_INTR_RXIS | AUART_INTR_RXIEN | AUART_INTR_TXIS | AUART_INTR_TXIEN));

        // The data is going out now.  Wait until it is completely transmitted

        __enable_interrupt();

        while(readReg(s->membase, AUART_STAT) & AUART_STAT_BUSY)
        {
          if(checkTimer(&xmitTimer))
            break;
        }

        serialFlush(portNumber);

        return;
}


int serialInt(void)
{
 int tempLength;
 REG32 stat, intr;
 MX_AUART_PORT *s = &port[0];
 uchar *p;

//  for(i = 0; i < NUMBER_OF_AUARTS; i++;
//  {

////////~!~!~!~!~      writeReg(s->membase, HW_ICOLL_VECTOR, 0);

      stat = readReg(s->membase, AUART_STAT);
      s->cts = (stat & AUART_STAT_CTS) ? TRUE : FALSE;
      intr = readReg(s->membase, AUART_INTR);

      switch(s->state)
      {
        case(IDLE):
/*
            if(intr & 0x00000ff2)
            {
              intr &= 0x00000ff2;
            }
            writeReg(s->membase, AUART_INTR_CLR, intr);
            flushSerial(0);
*/
            break;

            case(TRANSMITTING):
              // Anymore data to xmit>
              if((intr & AUART_INTR_TXIS) && (s->xmitCnt < s->xLength))
              {
                p = s->outBuffer + s->xmitCnt;
                tempLength = s->xLength - s->xmitCnt;

                while(!(readReg(s->membase, AUART_STAT) & AUART_STAT_TXFF))
                {
                  s->xmitCnt++;
                  writeReg(s->membase, AUART_DATA, *p++);
                  if(--tempLength)
                    continue;
                  else
                    break;
                }
              }

              if(s->xmitCnt != s->xLength)
              {
                writeReg(s->membase, AUART_INTR_SET, AUART_INTR_TXIEN);                 // Continue xmit
//                writeReg(s->membase, AUART_INTR_CLR, (AUART_INTR_RTIS | AUART_INTR_TXIS | AUART_INTR_RXIS | AUART_INTR_CTSMIS));
              }
              else
                writeReg(s->membase, AUART_INTR_CLR, AUART_INTR_TXIEN);                 // Turn off xmit interrupts

              break;

            case(RECEIVING):
              break;
      }

//  }

  return(0);
}

