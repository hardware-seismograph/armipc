#include "ipc/ipc.h"
#include "include/fs.h"
#include "../mach/mx28_pins.h"
#include "../mach/pinctrl.h"


#define MMC0_POWER	MXS_PIN_TO_GPIO(PINID_PWM3)
#define MMC1_POWER	MXS_PIN_TO_GPIO(PINID_PWM4)
#define MMC0_WP		MXS_PIN_TO_GPIO(PINID_SSP1_SCK)
#define MMC1_WP		MXS_PIN_TO_GPIO(PINID_GPMI_RESETN)

#define MMC2_POWER	MXS_PIN_TO_GPIO(PINID_AUART1_TX)

static int mxs_mmc_get_wp_ssp0(void)
{
	return gpio_get_value(MMC0_WP);
}

static int mxs_mmc_hw_init_ssp0(void)
{
 int ret = 0;

  /* Configure write protect GPIO pin */
///////////~!~!~!~!~!~  ret = gpio_request(MMC0_WP, "mmc0_wp");
  if (ret)
    goto out_wp;

///////////~!~!~!~!~!~!~!~!~!~!~!  gpio_set_value(MMC0_WP, 0);
//////////~!~!~!~!~!~!~!~  gpio_direction_input(MMC0_WP);

#if 0
  /* Configure POWER pin as gpio to drive power to MMC slot */
  ret = gpio_request(MMC0_POWER, "mmc0_power");
  if (ret)
    goto out_power;

  gpio_direction_output(MMC0_POWER, 0);
  mdelay(100);
#endif
  
  return 0;

#if 0
out_power:
#endif
	gpio_free(MMC0_WP);
out_wp:
	return ret;
}

#ifdef FOO
static void mxs_mmc_hw_release_ssp0(void)
{
#if 0
	gpio_free(MMC0_POWER);
#endif
	gpio_free(MMC0_WP);

}
#endif

#ifdef FOO
static void mxs_mmc_cmd_pullup_ssp0(int enable)
{
	mxs_set_pullup(PINID_SSP0_CMD, enable, "mmc0_cmd");
}
#endif

unsigned long mxs_mmc_setclock_ssp0(unsigned long hz)
{
#ifdef FOO
	unsigned long base = clk->parent->get_rate(clk->parent);
	unsigned long div = ((base/1000)  * 18) / (rate/1000);
	if (rate != ((base / div) * 18))
		return -EINVAL;
	if (clk->scale_reg == 0)
		return -EINVAL;
	base = __raw_readl(clk->scale_reg);
	base &= ~(0x3F << clk->scale_bits);
	base |= (div << clk->scale_bits);
	__raw_writel(base, clk->scale_reg);
	return 0;
#endif
  //    HW_CLKCTRL_SSP0_WR(BF_CLKCTRL_SSP0_DIV(40));

#ifdef NOT_USED
        struct clk *ssp = clk_get(NULL, "ssp.0"), *parent;

	if (hz > 1000000)
		parent = clk_get(NULL, "ref_io.0");
	else
		parent = clk_get(NULL, "xtal.0");

	clk_set_parent(ssp, parent);
	clk_set_rate(ssp, 2 * hz);
	clk_put(parent);
	clk_put(ssp);
#endif
        
	return hz;
}



short if_initInterface(hwInterface *file, eint8* opts)
{

//    mxcmci_init();
//  mxs_mmc_setclock_ssp0(450000);
//  mxs_mmc_hw_init_ssp0();
    file->sectorCount = 0;
    return(1);
}
/*****************************************************************************/

short if_writeBuf(hwInterface* file, unsigned long address, unsigned char* buf)
{
	return(0);
}
/*****************************************************************************/

short if_setPos(hwInterface* file,unsigned long address)
{
  file->sectorCount = address; 
  return(0);
}
/*****************************************************************************/

short if_readBuf(hwInterface* file,unsigned long address,unsigned char* buf)
{
	short r;

	r = sd_readSector(address, buf, 512);
	if(r!=0)printf("ERROR READING SECTOR %i\n",address);

	return(r);
}
/*****************************************************************************/
