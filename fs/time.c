#include "time.h"
#include "include/types.h"
#include "../ipc/ipc.h"

euint16 fs_makeDate(void)
{
 euint8 m,d;
 euint16 y;
 struct tm tm;
 
        RTC_read(&tm);
 
	y = tm.tm_year - 1980;
	m = tm.tm_mon;
	d = tm.tm_mday;
	
	return(
		(y>127?127<<9:(y&0x3F)<<9)   |
		((m==0||m>12)?1:(m&0xF)<<5)  |
		((d==0||d>31)?1:(d&0x1F))
	);
}

euint16 fs_makeTime(void)
{
 euint8 s,m,h;
 struct tm tm;

        RTC_read(&tm);

        s = tm.tm_sec;
	m = tm.tm_mon;
	h = tm.tm_hour;
	
	return(
		(h>23?0:(h&0x1F)<<11) |
		(m>59?0:(m&0x3F)<<5)  |
		(s>59?0:(s-s%2)/2)
	);
}

euint8 fs_hasTimeSupport(void)
{
	return(TRUE);
}

