/*
 * Copyright (C) 2009-2010 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
//#include <linux/init.h>
//#include <linux/platform_device.h>
//#include <linux/irq.h>
//#include <linux/gpio.h>
//#include <linux/delay.h>

#include <mach/pinctrl.h>
//#include "gpio.h"
#include "mx28_pins.h"



static struct pin_desc mx28evk_ssp0_pins[] = {
//#if defined(CONFIG_MMC_MXS) || defined(CONFIG_MMC_MXS_MODULE)
	/* Configurations of SSP0 SD/MMC port pins */
	{
	 .name	= "SSP0_DATA0",
	 .id	= PINID_SSP0_DATA0,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA1",
	 .id	= PINID_SSP0_DATA1,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA2",
	 .id	= PINID_SSP0_DATA2,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA3",
	 .id	= PINID_SSP0_DATA3,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
#if 0
	/* Only 4-bit mmc in our case... */
	{
	 .name	= "SSP0_DATA4",
	 .id	= PINID_SSP0_DATA4,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA5",
	 .id	= PINID_SSP0_DATA5,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA6",
	 .id	= PINID_SSP0_DATA6,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DATA7",
	 .id	= PINID_SSP0_DATA7,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
#endif // if 0
	{
	 .name	= "SSP0_CMD",
	 .id	= PINID_SSP0_CMD,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP0_DETECT",
	 .id	= PINID_SSP0_DETECT,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
	{
	 .name	= "SSP0_SCK",
	 .id	= PINID_SSP0_SCK,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_12MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
//#endif // defined(CONFIG_MMC_MXS) || defined(CONFIG_MMC_MXS_MODULE)
};

/* SSP2 */
static struct pin_desc mx28evk_ssp2_pins[] = {
//#if defined(CONFIG_MMC_MXS) || defined(CONFIG_MMC_MXS_MODULE)
	/* Configurations of SSP2 SD/MMC port pins */
	{
	 .name	= "SSP2_DATA0",
	 .id	= PINID_SSP2_MISO,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP2_DATA1",
	 .id	= PINID_SSP2_SS1,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP2_DATA2",
	 .id	= PINID_SSP2_SS2,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP2_DATA3",
	 .id	= PINID_SSP2_SS0,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP2_CMD",
	 .id	= PINID_SSP2_MOSI,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP2_DETECT",
	 .id	= PINID_AUART1_RX,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
	{
	 .name	= "SSP2_SCK",
	 .id	= PINID_SSP2_SCK,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_12MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
//#endif // defined(CONFIG_MMC_MXS) || defined(CONFIG_MMC_MXS_MODULE)
};

#if defined(CONFIG_FEC) || defined(CONFIG_FEC_MODULE)\
	|| defined(CONFIG_FEC_L2SWITCH)
static struct pin_desc mx28evk_eth_pins[] = {
	{
	 .name = "ENET0_MDC",
	 .id = PINID_ENET0_MDC,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_MDIO",
	 .id = PINID_ENET0_MDIO,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_RX_EN",
	 .id = PINID_ENET0_RX_EN,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_RXD0",
	 .id = PINID_ENET0_RXD0,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_RXD1",
	 .id = PINID_ENET0_RXD1,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_TX_EN",
	 .id = PINID_ENET0_TX_EN,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_TXD0",
	 .id = PINID_ENET0_TXD0,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET0_TXD1",
	 .id = PINID_ENET0_TXD1,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive	= 1,
	 },
	{
	 .name = "ENET1_RX_EN",
	 .id = PINID_ENET0_CRS,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET1_RXD0",
	 .id = PINID_ENET0_RXD2,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET1_RXD1",
	 .id = PINID_ENET0_RXD3,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET1_TX_EN",
	 .id = PINID_ENET0_COL,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET1_TXD0",
	 .id = PINID_ENET0_TXD2,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET1_TXD1",
	 .id = PINID_ENET0_TXD3,
	 .fun = PIN_FUN2,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_3_3V,
	 .drive = 1,
	 },
	{
	 .name = "ENET_CLK",
	 .id = PINID_ENET_CLK,
	 .fun = PIN_FUN1,
	 .strength = PAD_8MA,
	 .pull = 1,
	 .pullup = 1,
	 .voltage = PAD_1_8V,
	 .drive	= 1,
	 },
};
#endif

//static int __initdata enable_ssp1 = { 0 };
//static int __init ssp1_setup(char *__unused)
//{
//	enable_ssp1 = 1;
//	return 1;
//}

//__setup("ssp1", ssp1_setup);

static struct pin_desc mx28evk_ssp1_pins[] = {
	{
	 .name	= "SSP1_DATA0",
	 .id	= PINID_GPMI_D00,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA1",
	 .id	= PINID_GPMI_D01,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA2",
	 .id	= PINID_GPMI_D02,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA3",
	 .id	= PINID_GPMI_D03,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA4",
	 .id	= PINID_GPMI_D04,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA5",
	 .id	= PINID_GPMI_D05,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA6",
	 .id	= PINID_GPMI_D06,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DATA7",
	 .id	= PINID_GPMI_D07,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_CMD",
	 .id	= PINID_GPMI_RDY1,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 1,
	 .drive 	= 1,
	 .pull 		= 1,
	 },
	{
	 .name	= "SSP1_DETECT",
	 .id	= PINID_GPMI_RDY0,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
	{
	 .name	= "SSP1_SCK",
	 .id	= PINID_GPMI_WRN,
	 .fun	= PIN_FUN2,
	 .strength	= PAD_12MA,
	 .voltage	= PAD_3_3V,
	 .pullup	= 0,
	 .drive 	= 1,
	 .pull 		= 0,
	 },
};


//int enable_gpmi = { 0 };
//static int __init gpmi_setup(char *__unused)
//{
//	enable_gpmi = 1;
//	return 1;
//}

//__setup("gpmi", gpmi_setup);

static struct pin_desc mx28evk_gpmi_pins[] = {
	{
	 .name     = "GPMI D0",
	 .id       = PINID_GPMI_D00,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	.name     = "GPMI D1",
	.id       = PINID_GPMI_D01,
	.fun      = PIN_FUN1,
	.strength = PAD_4MA,
	.voltage  = PAD_3_3V,
	.pullup   = 0,
	.drive    = !0
	 },
	{
	 .name     = "GPMI D2",
	 .id       = PINID_GPMI_D02,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI D3",
	 .id       = PINID_GPMI_D03,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI D4",
	 .id       = PINID_GPMI_D04,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI D5",
	 .id       = PINID_GPMI_D05,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI D6",
	 .id       = PINID_GPMI_D06,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI D7",
	 .id       = PINID_GPMI_D07,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI CE0-",
	 .id       = PINID_GPMI_CE0N,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
//oskJanHln we don't use this for gpmi but 
#if 0
	{
	 .name     = "GPMI CE1-",
	 .id       = PINID_GPMI_CE1N,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
#endif
	{
	 .name     = "GPMI RDY0",
	 .id       = PINID_GPMI_RDY0,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI RDY1",
	 .id       = PINID_GPMI_RDY1,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI RD-",
	 .id       = PINID_GPMI_RDN,
	 .fun      = PIN_FUN1,
	 .strength = PAD_12MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI WR-",
	 .id       = PINID_GPMI_WRN,
	 .fun      = PIN_FUN1,
	 .strength = PAD_12MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI ALE",
	 .id       = PINID_GPMI_ALE,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI CLE",
	 .id       = PINID_GPMI_CLE,
	 .fun      = PIN_FUN1,
	 .strength = PAD_4MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
	{
	 .name     = "GPMI RST-",
	 .id       = PINID_GPMI_RESETN,
	 .fun      = PIN_FUN1,
	 .strength = PAD_12MA,
	 .voltage  = PAD_3_3V,
	 .pullup   = 0,
	 .drive    = !0
	 },
};

#if defined(CONFIG_SPI_MXS) || defined(CONFIG_SPI_MXS_MODULE)
static struct pin_desc mx28evk_spi_pins[] = {
	{
	 .name	= "SSP2 MOSI",
	 .id	= PINID_SSP2_MOSI,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP2 MISO",
	 .id	= PINID_SSP2_MISO,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP2 SCK",
	 .id	= PINID_SSP2_SCK,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP2 SS0",
	 .id	= PINID_SSP2_SS0,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
};
#endif

#if defined(CONFIG_SPI_MXS) || defined(CONFIG_SPI_MXS_MODULE)
static struct pin_desc mx28evk_spi_pins_ssp0[] = {
	{
	 .name	= "SSP0 MOSI",
	 .id	= PINID_SSP0_CMD,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP0 MISO",
	 .id	= PINID_SSP0_DATA0,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP0 SCK",
	 .id	= PINID_SSP0_SCK,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_4MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
	{
	 .name	= "SSP0 SS0",
	 .id	= PINID_SSP0_DATA3,
	 .fun	= PIN_FUN1,
	 .strength	= PAD_8MA,
	 .voltage	= PAD_3_3V,
	 .drive 	= 1,
	 },
};
#endif

//oskDannyN
#ifdef CONFIG_MXS_AUART0_DEVICE_ENABLE
static struct pin_desc mx28evk_auart0_pins[] = {
	{
	 .name  = "AUART0.RX",
	 .id    = PINID_AUART0_RX,
	 .fun   = PIN_FUN1,
	 },
	{
	 .name  = "AUART0.TX",
	 .id    = PINID_AUART0_TX,
	 .fun   = PIN_FUN1,
	 },
};
#endif

#ifdef CONFIG_MXS_AUART1_DEVICE_ENABLE
static struct pin_desc mx28evk_auart1_pins[] = {
	{
	 .name  = "AUART1.RX",
	 .id    = PINID_AUART1_RX,
	 .fun   = PIN_FUN1,
	 },
	{
	 .name  = "AUART1.TX",
	 .id    = PINID_AUART1_TX,
	 .fun   = PIN_FUN1,
	 },
};
#endif

#ifdef CONFIG_MXS_AUART2_DEVICE_ENABLE
static struct pin_desc mx28evk_auart2_pins[] = {
	{
	 .name  = "AUART2.RX",
	 .id    = PINID_AUART2_RX,
	 .fun   = PIN_FUN1,
	 },
	{
	 .name  = "AUART2.TX",
	 .id    = PINID_AUART2_TX,
	 .fun   = PIN_FUN1,
	 },
};
#endif

#ifdef CONFIG_MXS_AUART3_DEVICE_ENABLE
static struct pin_desc mx28evk_auart3_pins[] = {
	{
	 .name  = "AUART3.RX",
	 .id    = PINID_AUART3_RX,
	 .fun   = PIN_FUN1,
	 },
	{
	 .name  = "AUART3.TX",
	 .id    = PINID_AUART3_TX,
	 .fun   = PIN_FUN1,
	 },
};
#endif

#ifdef CONFIG_MXS_AUART4_DEVICE_ENABLE
static struct pin_desc mx28evk_auart4_pins[] = {
	{
	 .name  = "AUART4.RX",
	 .id    = PINID_SAIF0_BITCLK,
	 .fun   = PIN_FUN3,
	 },
	{
	 .name  = "AUART4.TX",
	 .id    = PINID_SAIF0_SDATA0,
	 .fun   = PIN_FUN3,
	 },
};
#endif
//oskdannyn

#if defined(CONFIG_FEC) || defined(CONFIG_FEC_MODULE)\
	|| defined(CONFIG_FEC_L2SWITCH)
int mx28evk_enet_gpio_init(void)
{
	/* pwr */
	// gpio_request(MXS_PIN_TO_GPIO(PINID_SSP1_DATA3), "ENET_PWR");
	// gpio_direction_output(MXS_PIN_TO_GPIO(PINID_SSP1_DATA3), 0);

	/* reset phy */
	gpio_request(MXS_PIN_TO_GPIO(PINID_LCD_D07), "PHY_RESET");
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_LCD_D07), 0);

	/*
	 * Before timer bug fix(set wrong match value of timer),
	 * mdelay(10) delay 50ms actually.
	 * So change delay to 50ms after timer issue fix.
	 */
	mdelay(50);
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_LCD_D07), 1);

	return 0;
}

void mx28evk_enet_io_lowerpower_enter(void)
{
	int i;
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_SSP1_DATA3), 1);
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_ENET0_RX_CLK), 0);
	gpio_request(MXS_PIN_TO_GPIO(PINID_ENET0_TX_CLK), "ETH_INT");
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_ENET0_TX_CLK), 0);

	for (i = 0; i < ARRAY_SIZE(mx28evk_eth_pins); i++) {
		mxs_release_pin(mx28evk_eth_pins[i].id,
			mx28evk_eth_pins[i].name);
		gpio_request(MXS_PIN_TO_GPIO(mx28evk_eth_pins[i].id),
			mx28evk_eth_pins[i].name);
		gpio_direction_output(
			MXS_PIN_TO_GPIO(mx28evk_eth_pins[i].id), 0);
	}

}

void mx28evk_enet_io_lowerpower_exit(void)
{
	int i;
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_SSP1_DATA3), 0);
	gpio_direction_output(MXS_PIN_TO_GPIO(PINID_ENET0_RX_CLK), 1);
	gpio_free(MXS_PIN_TO_GPIO(PINID_ENET0_TX_CLK));
	for (i = 0; i < ARRAY_SIZE(mx28evk_eth_pins); i++) {
		gpio_free(MXS_PIN_TO_GPIO(mx28evk_eth_pins[i].id));
		mxs_request_pin(mx28evk_eth_pins[i].id,
			mx28evk_eth_pins[i].fun,
			mx28evk_eth_pins[i].name);
	}
}

#else
int mx28evk_enet_gpio_init(void)
{
	return 0;
}
void mx28evk_enet_io_lowerpower_enter(void)
{}
void mx28evk_enet_io_lowerpower_exit(void)
{}
#endif

void mx28evk_init_pin_group(struct pin_desc *pins, unsigned count)
{
	int i;
	struct pin_desc *pin;
	for (i = 0; i < count; i++) {
		pin = pins + i;
#ifdef FOO
		if (pin->fun == PIN_GPIO)
			gpio_request(MXS_PIN_TO_GPIO(pin->id), pin->name);
		else
			mxs_request_pin(pin->id, pin->fun, pin->name);
#endif
		if (pin->drive) {
			mxs_set_strength(pin->id, pin->strength, pin->name);
			mxs_set_voltage(pin->id, pin->voltage, pin->name);
		}
		if (pin->pull)
			mxs_set_pullup(pin->id, pin->pullup, pin->name);
		if (pin->fun == PIN_GPIO) {
			if (pin->output)
				gpio_direction_output(MXS_PIN_TO_GPIO(pin->id),
							pin->data);
			else
				gpio_direction_input(MXS_PIN_TO_GPIO(pin->id));
		}
	}
}

#ifdef FOO
void mx28evk_release_pin_group(struct pin_desc *pins, unsigned count)
{
	int i;
	struct pin_desc *pin;

	for (i = 0; i < count; i++) {
		pin = pins + i;
		if (pin->fun == PIN_GPIO)
			gpio_free(MXS_PIN_TO_GPIO(pin->id));
		else
			mxs_release_pin(pin->id, pin->name);
	}
}
#endif

/*--------------------------------------------------------------------------
 * Function:    mx28_init_uart_gpio
 * Parameters:  0-4 = AUART0...AUART4
 * Returns:     0: unknown uart, 1: tried to config uart
 * Description: Calls FREESCALE functions to init the GPIO for a particular
 *              UART. Needed for multi function expansion boards
 *-------------------------------------------------------------------------*/
static int mx28_init_auart_gpio(int auart)
{
	switch (auart)
	{
#ifdef CONFIG_MXS_AUART0_DEVICE_ENABLE
		case 0: mx28evk_init_pin_group(mx28evk_auart0_pins, ARRAY_SIZE(mx28evk_auart0_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART1_DEVICE_ENABLE
		case 1: mx28evk_init_pin_group(mx28evk_auart1_pins, ARRAY_SIZE(mx28evk_auart1_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART2_DEVICE_ENABLE
		case 2: mx28evk_init_pin_group(mx28evk_auart2_pins, ARRAY_SIZE(mx28evk_auart2_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART3_DEVICE_ENABLE
		case 3: mx28evk_init_pin_group(mx28evk_auart3_pins, ARRAY_SIZE(mx28evk_auart3_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART4_DEVICE_ENABLE
		case 4: mx28evk_init_pin_group(mx28evk_auart4_pins, ARRAY_SIZE(mx28evk_auart4_pins)); break;
#endif
		default: return (1);
	}
	return (0);
}

#ifdef FOO
/*--------------------------------------------------------------------------
 * Function:    mx28_release_uart_gpio
 * Parameters:  0-4 = AUART0...AUART4
 * Returns:     0: unknown uart, 1: tried to config uart
 * Description: Calls FREESCALE functions to release the GPIO for a particular
 *              UART. Needed for multi function expansion boards
 *-------------------------------------------------------------------------*/
static int mx28_release_auart_gpio(int auart)
{
	switch (auart)
	{
#ifdef CONFIG_MXS_AUART0_DEVICE_ENABLE
		case 0: mx28evk_release_pin_group(mx28evk_auart0_pins, ARRAY_SIZE(mx28evk_auart0_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART1_DEVICE_ENABLE
		case 1: mx28evk_release_pin_group(mx28evk_auart1_pins, ARRAY_SIZE(mx28evk_auart1_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART2_DEVICE_ENABLE
		case 2: mx28evk_release_pin_group(mx28evk_auart2_pins, ARRAY_SIZE(mx28evk_auart2_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART3_DEVICE_ENABLE
		case 3: mx28evk_release_pin_group(mx28evk_auart3_pins, ARRAY_SIZE(mx28evk_auart3_pins)); break;
#endif
#ifdef CONFIG_MXS_AUART4_DEVICE_ENABLE
		case 4: mx28evk_release_pin_group(mx28evk_auart4_pins, ARRAY_SIZE(mx28evk_auart4_pins)); break;
#endif
		default: return (1);
	}
	return (0);
}
#endif
/*--------------------------------------------------------------------------
 * Function:    mx28_init_sd_mmc
 * Parameters:  0 or 2 = 0 == SSP0 / 2 = SSP2
 * Returns:     0: unknown id, 1: tried to config ssp
 * Description: Calls FREESCALE functions to init the GPIO for a particular
 *              UART. Needed for multi function expansion boards
 *-------------------------------------------------------------------------*/
int mx28_init_sd_mmc (int intf)
{
#if defined(CONFIG_MMC_MXS) || defined(CONFIG_MMC_MXS_MODULE)

	switch (intf)
	{
		case 0: mx28evk_init_pin_group(mx28evk_ssp0_pins, ARRAY_SIZE(mx28evk_ssp0_pins)); break;
		case 2: mx28evk_init_pin_group(mx28evk_ssp2_pins, ARRAY_SIZE(mx28evk_ssp2_pins)); break;
		default: return (0);
	}
	return (1);
#endif
 return 0;
}
//EXPORT_SYMBOL(mx28_init_sd_mmc);

/*--------------------------------------------------------------------------
 * Function:    mx28_init_spi
 * Parameters:  0: use SSP0, 1, use SSP1 (not implemented), 2: use SSP2
 * Returns:     0: init failed, 1: init succeeded
 * Description: Calls FREESCALE functions to init the GPIO for SSPX
 *              SPI. Needed for multi function expansion boards
 *-------------------------------------------------------------------------*/
int mx28_init_spi (int intf)
{
#if defined(CONFIG_SPI_MXS) || defined(CONFIG_SPI_MXS_MODULE)
	switch (intf)
	{
		case 0:
			mx28evk_init_pin_group(mx28evk_spi_pins_ssp0, ARRAY_SIZE(mx28evk_spi_pins_ssp0));
			break;
		case 2:
			mx28evk_init_pin_group(mx28evk_spi_pins, ARRAY_SIZE(mx28evk_spi_pins));
			break;
		case 1:
		default:
			return 0;
	}
#endif
 return 1;
}
//EXPORT_SYMBOL(mx28_init_spi);


/*--------------------------------------------------------------------------
 * Function:    mx28_init_uart_control_pins
 * Parameters:  uart_COMM_TYPE (0 = DTE, 1 DCE, 2 = RS485, 3 = RS422)
 * 				uart_DTR
 * 				uart_DCD
 * 				uart_DSR
 * 				uart_RI
 * 				uart_MBAUD
 * 				uart_RE (Receive enable for RS485/RE422
 * 				uart_DE (Drive enable for RS485/RE422
 * Returns:     0:	OK
 * 				-1:	failed config uart
 * 				-2: invalid dev-id
 * 				-3:	invalid COMM_TYPE
 * Description: Calls FREESCALE functions to init the GPIO for a particular
 *              UART. Needed for multi function expansion boards
 *-------------------------------------------------------------------------*/
int mx28_init_uart_control_pins (int devid, int uart_COMM_TYPE, int uart_RTSCTS, int uart_DTR,
									int uart_DCD, int uart_DSR, int uart_RI,
									int uart_MBAUD, int uart_DE, int uart_RE)
{
	struct pin_desc tUartCtrlPins[6];
	int nInd = 0;

	if (mx28_init_auart_gpio (devid)) {
		return -1;
	}

	memset (tUartCtrlPins, 0, sizeof (tUartCtrlPins));

	if (uart_RTSCTS && (uart_COMM_TYPE == 0 || uart_COMM_TYPE == 1)) {

		switch (devid)
		{
			case 0:
				tUartCtrlPins[0].name = "AUART0.CTS";
				tUartCtrlPins[0].id = PINID_AUART0_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART0.RTS";
				tUartCtrlPins[1].id = PINID_AUART0_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 1:
				tUartCtrlPins[0].name = "AUART1.CTS";
				tUartCtrlPins[0].id = PINID_AUART1_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART1.RTS";
				tUartCtrlPins[1].id = PINID_AUART1_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 2:
				tUartCtrlPins[0].name = "AUART2.CTS";
				tUartCtrlPins[0].id = PINID_AUART2_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART2.RTS";
				tUartCtrlPins[1].id = PINID_AUART2_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 3:
				tUartCtrlPins[0].name = "AUART3.CTS";
				tUartCtrlPins[0].id = PINID_AUART3_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART3.RTS";
				tUartCtrlPins[1].id = PINID_AUART3_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 4:
				tUartCtrlPins[0].name = "AUART4.CTS";
				tUartCtrlPins[0].id = PINID_SAIF0_MCLK;
				tUartCtrlPins[0].fun = PIN_FUN3;

				tUartCtrlPins[1].name = "AUART4.RTS";
				tUartCtrlPins[1].id = PINID_SAIF0_LRCLK;
				tUartCtrlPins[1].fun = PIN_FUN3;
				break;

			default:
				return -2;
		}

		nInd = 2;
	}

	switch (uart_COMM_TYPE) {
		case 0:
			/* uart is DTE
			 * DTR: output
			 * DCD: input
			 * DSR: input
			 * RI: input
			 */
			if (uart_DTR >= 0) {
				tUartCtrlPins[nInd].name = "DTR";
				tUartCtrlPins[nInd].id = uart_DTR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_DCD >= 0) {
				tUartCtrlPins[nInd].name = "DCD";
				tUartCtrlPins[nInd].id = uart_DCD;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_DSR >= 0) {
				tUartCtrlPins[nInd].name = "DSR";
				tUartCtrlPins[nInd].id = uart_DSR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_RI >= 0) {
				tUartCtrlPins[nInd].name = "RI";
				tUartCtrlPins[nInd].id = uart_RI;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			break;

		case 1:
			/* uart is DCE
			 * DTR: input
			 * DCD: output
			 * DSR: output
			 * RI: output
			*/
			if (uart_DTR >= 0) {
				tUartCtrlPins[nInd].name = "DTR";
				tUartCtrlPins[nInd].id = uart_DTR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_DCD >= 0) {
				tUartCtrlPins[nInd].name = "DCD";
				tUartCtrlPins[nInd].id = uart_DCD;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_DSR >= 0) {
				tUartCtrlPins[nInd].name = "DSR";
				tUartCtrlPins[nInd].id = uart_DSR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_RI >= 0) {
				tUartCtrlPins[nInd].name = "RI";
				tUartCtrlPins[nInd].id = uart_RI;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			break;
		case 2:
		case 3:
		case 4: /* RS485 in combination with the cortex */
			if (uart_RE >= 0)
			{
				tUartCtrlPins[nInd].name = "RE";
				tUartCtrlPins[nInd].id = uart_RE;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
 			if (uart_DE >= 0)
			{
				tUartCtrlPins[nInd].name = "DE";
				tUartCtrlPins[nInd].id = uart_DE;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			break;
		default:
			return -3;
	}

	mx28evk_init_pin_group (tUartCtrlPins, nInd);

	return 0;
}
//EXPORT_SYMBOL(mx28_init_uart_control_pins);

int mx28_release_uart_control_pins (int devid, int uart_COMM_TYPE, int uart_RTSCTS, int uart_DTR,
									int uart_DCD, int uart_DSR, int uart_RI,
									int uart_MBAUD, int uart_DE, int uart_RE)
{
	struct pin_desc tUartCtrlPins[6];
	int nInd = 0;

	if (mx28_release_auart_gpio (devid)) {
		return -1;
	}

	memset (tUartCtrlPins, 0, sizeof (tUartCtrlPins));

	if (uart_RTSCTS && (uart_COMM_TYPE == 0 || uart_COMM_TYPE == 1)) {

		switch (devid)
		{
			case 0:
				tUartCtrlPins[0].name = "AUART0.CTS";
				tUartCtrlPins[0].id = PINID_AUART0_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART0.RTS";
				tUartCtrlPins[1].id = PINID_AUART0_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 1:
				tUartCtrlPins[0].name = "AUART1.CTS";
				tUartCtrlPins[0].id = PINID_AUART1_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART1.RTS";
				tUartCtrlPins[1].id = PINID_AUART1_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 2:
				tUartCtrlPins[0].name = "AUART2.CTS";
				tUartCtrlPins[0].id = PINID_AUART2_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART2.RTS";
				tUartCtrlPins[1].id = PINID_AUART2_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 3:
				tUartCtrlPins[0].name = "AUART3.CTS";
				tUartCtrlPins[0].id = PINID_AUART3_CTS;
				tUartCtrlPins[0].fun = PIN_FUN1;

				tUartCtrlPins[1].name = "AUART3.RTS";
				tUartCtrlPins[1].id = PINID_AUART3_RTS;
				tUartCtrlPins[1].fun = PIN_FUN1;
				break;

			case 4:
				tUartCtrlPins[0].name = "AUART4.CTS";
				tUartCtrlPins[0].id = PINID_SAIF0_MCLK;
				tUartCtrlPins[0].fun = PIN_FUN3;

				tUartCtrlPins[1].name = "AUART4.RTS";
				tUartCtrlPins[1].id = PINID_SAIF0_LRCLK;
				tUartCtrlPins[1].fun = PIN_FUN3;
				break;

			default:
				return -2;
		}

		nInd = 2;
	}

	switch (uart_COMM_TYPE) {
		case 0:
			/* uart is DTE
			 * DTR: output
			 * DCD: input
			 * DSR: input
			 * RI: input
			 */
			if (uart_DTR >= 0) {
				tUartCtrlPins[nInd].name = "DTR";
				tUartCtrlPins[nInd].id = uart_DTR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_DCD >= 0) {
				tUartCtrlPins[nInd].name = "DCD";
				tUartCtrlPins[nInd].id = uart_DCD;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_DSR >= 0) {
				tUartCtrlPins[nInd].name = "DSR";
				tUartCtrlPins[nInd].id = uart_DSR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_RI >= 0) {
				tUartCtrlPins[nInd].name = "RI";
				tUartCtrlPins[nInd].id = uart_RI;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			break;

		case 1:
			/* uart is DCE
			 * DTR: input
			 * DCD: output
			 * DSR: output
			 * RI: output
			*/
			if (uart_DTR >= 0) {
				tUartCtrlPins[nInd].name = "DTR";
				tUartCtrlPins[nInd].id = uart_DTR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				nInd++;
			}
			if (uart_DCD >= 0) {
				tUartCtrlPins[nInd].name = "DCD";
				tUartCtrlPins[nInd].id = uart_DCD;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_DSR >= 0) {
				tUartCtrlPins[nInd].name = "DSR";
				tUartCtrlPins[nInd].id = uart_DSR;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			if (uart_RI >= 0) {
				tUartCtrlPins[nInd].name = "RI";
				tUartCtrlPins[nInd].id = uart_RI;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			break;
		case 2:
		case 3:
		case 4: /* RS485 in combination with the cortex */
			if (uart_RE >= 0)
			{
				tUartCtrlPins[nInd].name = "RE";
				tUartCtrlPins[nInd].id = uart_RE;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
 			if (uart_DE >= 0)
			{
				tUartCtrlPins[nInd].name = "DE";
				tUartCtrlPins[nInd].id = uart_DE;
				tUartCtrlPins[nInd].fun = PIN_GPIO;
				tUartCtrlPins[nInd].output = 1;
				tUartCtrlPins[nInd].drive = 1;
				tUartCtrlPins[nInd].voltage = PAD_3_3V;
				tUartCtrlPins[nInd].strength = PAD_4MA;
				nInd++;
			}
			break;
		default:
			return -3;
	}

	mx28evk_release_pin_group (tUartCtrlPins, nInd);

	return 0;
}
//EXPORT_SYMBOL(mx28_release_uart_control_pins);


int mx28_init_i2c_bus_pins ( int bus )
{
	struct pin_desc tI2c_1_Pins[2] =
	{
		{
			.name = "I2C0_SCL",
			.id = PINID_I2C0_SCL,
			.fun = PIN_FUN1,
			.strength = PAD_8MA,
			.voltage = PAD_3_3V,
			.drive	= 1,
		},
		{
			.name = "I2C0_SDA",
			.id = PINID_I2C0_SDA,
			.fun = PIN_FUN1,
			.strength = PAD_8MA,
			.voltage = PAD_3_3V,
			.drive	= 1,
		}
	};
	struct pin_desc tI2c_2_Pins[2] =
	{
		{
			.name = "I2C1_SCL",
			.id = PINID_PWM0,
			.fun = PIN_FUN2,
			.strength = PAD_8MA,
			.voltage = PAD_3_3V,
			.drive	= 1,
		},
		{
			.name = "I2C1_SDA",
			.id = PINID_PWM1,
			.fun = PIN_FUN2,
			.strength = PAD_8MA,
			.voltage = PAD_3_3V,
			.drive	= 1,
		}
	};

	if (bus == 0)
	{
		mx28evk_init_pin_group ( tI2c_1_Pins,
								 ARRAY_SIZE ( tI2c_1_Pins ) );
	}
	else if (bus == 1)
	{
		mx28evk_init_pin_group ( tI2c_2_Pins,
								 ARRAY_SIZE ( tI2c_2_Pins ) );
	}

	return 0;
}
//EXPORT_SYMBOL ( mx28_init_i2c_bus_pins );

/*--------------------------------------------------------------------------
 * Function:    mx28_disable_duart_pins
 * Parameters:  n/a
 * Returns:     0: call failed, 1: call succeeded
 * Description: Calls FREESCALE functions to release the GPIO for DUART pins.
 *-------------------------------------------------------------------------*/
int mx28_disable_duart_pins (void)
{
#if defined(CONFIG_SERIAL_MXS_DUART) || \
	defined(CONFIG_SERIAL_MXS_DUART_MODULE)
	int i;

	// Release all pins
	for (i = 0; i < ARRAY_SIZE(mx28evk_duart_pins); i++) {
		mxs_release_pin(mx28evk_duart_pins[i].id, mx28evk_duart_pins[i].name);
	}
	// Reinit pins as IO
	mx28evk_init_pin_group(mx28evk_duart_io_pins, ARRAY_SIZE(mx28evk_duart_io_pins));
	// And release them one last time
	for (i = 0; i < ARRAY_SIZE(mx28evk_duart_io_pins); i++) {
		gpio_free(MXS_PIN_TO_GPIO(mx28evk_duart_io_pins[i].id));
	}
        return 1;
#endif
 return 0;
}
//EXPORT_SYMBOL(mx28_disable_duart_pins);

/*--------------------------------------------------------------------------
 * Function:    mx28_release_usb_otg_id_pin
 * Parameters:  n/a
 * Returns:     0: call failed, 1: call succeeded
 * Description: Calls FREESCALE functions to release the GPIO for USB_OTG_ID.
 *-------------------------------------------------------------------------*/
int mx28_release_usb_otg_id_pin (void)
{
#if defined(CONFIG_USB_OTG)
	int i = 0;
	struct pin_desc usb0_otg_id_gpio =
	 {
	 .name   = "usb0_id",
	 .id     = PINID_PWM2,
	 .fun    = PIN_GPIO,
	 .data   = 1,
	 .output = 1,
	 };

	// search the pin
	while ( (i<ARRAY_SIZE(mx28evk_fixed_pins)) && (mx28evk_fixed_pins[i].id != PINID_PWM2) ) {
		++i;
	}
	if (mx28evk_fixed_pins[i].id == PINID_PWM2) {
		mxs_release_pin(mx28evk_fixed_pins[i].id, mx28evk_fixed_pins[i].name);
		// Reinit pin as IO
		mx28evk_init_pin_group(&usb0_otg_id_gpio, 1);
		// And release them one last time
		gpio_free(MXS_PIN_TO_GPIO(usb0_otg_id_gpio.id));
	}
	return 1;
#endif
 return 0;
}
//EXPORT_SYMBOL(mx28_release_usb_otg_id_pin);

void  mx28evk_pins_init(void)
{

#ifdef FOO
      mx28evk_init_pin_group(mx28evk_fixed_pins,
						ARRAY_SIZE(mx28evk_fixed_pins));
#endif
      
#if defined(CONFIG_SERIAL_MXS_DUART) || \
	defined(CONFIG_SERIAL_MXS_DUART_MODULE)
	mx28evk_init_pin_group(mx28evk_duart_pins,
						ARRAY_SIZE(mx28evk_duart_pins));
#endif
#ifdef FOO
	if (enable_ssp1) {
		pr_info("Initializing SSP1 pins\n");
		mx28evk_init_pin_group(mx28evk_ssp1_pins,
						ARRAY_SIZE(mx28evk_ssp1_pins));
	} else if (enable_gpmi) {
		pr_info("Initializing GPMI pins\n");
		mx28evk_init_pin_group(mx28evk_gpmi_pins,
						ARRAY_SIZE(mx28evk_gpmi_pins));
	}
#endif
        
        
#if defined(CONFIG_FEC) || defined(CONFIG_FEC_MODULE)\
	|| defined(CONFIG_FEC_L2SWITCH)
		mx28evk_init_pin_group(mx28evk_eth_pins,
						ARRAY_SIZE(mx28evk_eth_pins));
#endif
}
