/* NETZ.C - basic tcp interface for lwip

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary 
work and also under the United States Copyright Act of 1976, as amended. 
Distribution and access are limited only to authorized persons. Disclosure, 
reproduction, distribution or use, such as use to prepare derivative works, 
may violate federal andn state laws.
*/

/* HISTORY
11-02-08 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifdef FOOOOOOOOOOOO
#include "version.h"
#include "tern.h"

#include "lwip/tcp.h"
#include "lwip/api.h"
#endif

#include "ipc.h"

#define kSUCCESS  0
#define kFAILURE -1

#ifdef FOOOOOOOOo
typedef struct {
    struct netconn * s;
    struct netbuf * qp;
    int             qo;
} mysocket, * SOCKET;
#endif

SOCKET net_socket (void)
{
    struct netconn * nc;
    SOCKET ns;

    ns = mem_malloc (sizeof(mysocket));
    if (ns) {
//~!~!~!~!~!        nc = netconn_new (NETCONN_TCP);
        if (nc) {
            ns->s = nc;
            ns->qo = 0;
            ns->qp = NULL;
            return ns;
        }
        mem_free (ns);
    }
    return NULL;
}

SOCKET net_accept (SOCKET s)
{
    struct netconn * nc;
    SOCKET ns;

    if  (s && s->s) {
        nc = netconn_accept (s->s);
        if (nc) {
            ns = mem_malloc (sizeof(mysocket));
            if (ns) {
                ns->s = nc;
                ns->qo = 0;
                ns->qp = NULL;
                return ns;
            }
///////~!~!~!~!            netconn_delete (nc);
        }
    }
    return NULL;
}

int net_delete (SOCKET s)
{
    if (s) {
#ifdef FOOOOOOOOO
        if (s->qp)
            netbuf_delete (s->qp);
        if (s->s)
            netconn_delete (s->s);
#endif
        mem_free (s);
    }
    return kSUCCESS;
}

int net_timeout (SOCKET s, int timeout)
{
    if (s && s->s) {
///////~!~!~!~!        s->s->recv_timeout = timeout;
        return kSUCCESS;
    }
    return kFAILURE;
}

int net_bind (SOCKET s, struct ip_addr * addr, u16_t port)
{
    return (s && s->s) ? netconn_bind (s->s, addr, port) : kFAILURE;
}

int net_connect (SOCKET s, struct ip_addr * addr, u16_t port)
{
//~!~!~!~!~    return (s && s->s) ? netconn_connect (s->s, addr, port) : kFAILURE;
return(1);
}

int net_getaddr (SOCKET s, struct ip_addr *  addr, u16_t * port, int local)
{
    return (s && s->s) ? netconn_getaddr (s->s, addr, port, local) : kFAILURE;
}

int net_close (SOCKET s)
{
//~!~!~!~!~    return (s && s->s) ? netconn_close (s->s) : kFAILURE;
return(1);
}

int net_listen (SOCKET s)
{
    return (s && s->s) ? netconn_listen (s->s) : kFAILURE;
}

int net_get (SOCKET s, char * m, int n)
{
    int i, u, z;

    if (s && s->s && m) {

        z = 0;
        while (n > 0) {

            if (! s->qp) {
                s->qo = 0;
/////~!~!~!~!~!~                s->qp = netconn_recv (s->s);
                // treat null as timeout
                if (! s->qp) return kFAILURE;
            }

//////~!~!~!~!~!~!            u = netbuf_len (s->qp) - s->qo;
            i = (u > n) ? n : u;

///////~!~!~!~!~!~            netbuf_copy_partial (s->qp, m, i, s->qo);

            m += i;
            n -= i;
            z += i;

            // ??? deliver right away, so no loop
//~!~!~!~!~!~!~!            if (s->qp->p->flags & PBUF_FLAG_PUSH) 
//~!~!~!~!~!~!~!                n = 0;

            if (! (u > i)) {
/////////~!~!!@~!~!~!~!~                netbuf_delete (s->qp);
                s->qp = NULL;
                s->qo = 0;
            }
            else {
                s->qo += i;
            }

            // nothing waiting, we're done
            if (! s->s->recv_avail) break;
        }
        return z;
    }

    // bad argument
    return kFAILURE;
}

int net_gets (SOCKET s, char * m, int n)
{
    int e;

    if (s && s->s && m) {

        while (--n > 0) {

            e = net_get (s, m, 1);
            if (e <= 0) break;

            e = * m;
            if (e == 0x0D) break;
            if (e == 0x0A) break;
            if (e == 0x09) * m = 0x20;
            m++;
        }
        * m = 0;

        return e;
    }

    // bad argument
    return kFAILURE;
}

void net_put (SOCKET s, const char * m, int n)
{
    if (s && s->s && m) {
//~!~!~!~!~!        netconn_write (s->s, m, n, NETCONN_COPY | NETCONN_MORE);
    }
}

void net_puts (SOCKET s, const char * m)
{
    if (s && s->s && m) {
//~!~!~!~!~!        netconn_write (s->s, m, strlen(m), NETCONN_COPY);
//~!~!~!~!~!        netconn_write (s->s, "\r\n", 2, NETCONN_COPY | NETCONN_MORE);
    }
}

// --------------------------------------------------------------------------

int net_WaitHack (SOCKET s, int t)
{
    t *= 5;  // timeout, t, seconds to loop delay
    while (s->s->pcb.tcp->unsent || s->s->pcb.tcp->unacked) {
        if (--t < 0) break;
        vTaskDelay (200L);
    }
    return (t < 0) ? kFAILURE : kSUCCESS;
}

// --------------------------------------------------------------------------

static unsigned char * share_area;
static int share_size;

unsigned char * net_share_init (int size)
{
    share_area = (unsigned char *)malloc(size);
    share_size = share_area ? size : 0;
    return share_area;
}

unsigned char * net_share_area (void)
{
    return share_area;
}

int net_share_size (void)
{
    return share_size;
}

int net_share_quit (void)
{
    free (share_area);
    share_area = NULL;
    share_size = 0;
    return kSUCCESS;
}

