/* RTOS.H - newlife project

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary 
work and also under the United States Copyright Act of 1976, as amended. 
Distribution and access are limited only to authorized persons. Disclosure, 
reproduction, distribution or use, such as use to prepare derivative works, 
may violate federal andn state laws.
*/

/* HISTORY
12-01-09 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifndef __RTOS_H__
#define __RTOS_H__

#if ((defined(TERN_20Mhz) + defined(TERN_40Mhz)) != 1)
#error "only one of TERN_20Mhz or TERN_40Mhz must be defined"
#endif

#ifdef TERN_20Mhz
#include "freertos/FreeRTOS_20Mhz.h"
#endif

#ifdef TERN_40Mhz
#include "freertos/FreeRTOS_40Mhz.h"
#endif

#include "freertos/semphr.h"
#include "freertos/task.h"

#endif
