/*  BRIDGE.C - tern/3000 removable card and (printer interface)
    for the newlife project. Derivative work of 3000PR
*/

/*  NOTICE
    Copyright (c) 2018 by Geosonics Inc.  All rights reserved.
    This is a generally unpublished work which is protected both as a
    proprietary work and also under the United States Copyright Act
    of 1976, as amended.  Distribution and access are limited only to
    authorized persons.  Disclosure, reproduction, distribution or use,
    such as use to prepare derivative works, may violate federal and
    state laws.
*/


#include "ipc.h"

#define USE_INSITU_LOGFILE
#define NO_PRINTER_INTERFACE
#define USE_DYNAMIC_MEMORY
//#define USE_DYNAMIC_MEMORY_BUFFER

/* HISTORY
   10/25/05 initial implementation (00.00) (dv)
   05/11/06 ported for tern board (00.00) (dv)
   10/02/06 fixed CPY command (00.01) (dv)
   10/11/06 added VER command
            increased print buffer size to 1K
            set version string (00.02) (dv)
   10/15/06 added flush (drain) on printer close
            changed PRN command from "pass-thru"
            to full xon/xoff flow control, reduced
            buffer size, tried for tighter code
            set version string (00.03) (dv)
   10/17/06 fixed xon/xoff flow control
            set version string (00.04) (dv)
   10/18/06 reduced output buffer size on printer port
            set version string (00.05) (dv)
   10/19/06 changed xxx_read and xxx_write to use low
            level unbuffered block read and write functions
            changed (reversed) calculation in sio_used
            set version string (00.06) (dv)
   10/23/06 fixed sio_used
            changed *_putC routines to not use "safetyloop"
            added debugging code to mfs_load
            set version string (00.07) (dv)
   10/24/06 added debug write back test for cpy command
            (added debug code to cmd_cpy and mfs_save)
            changed sio_used to use serhit1
            set version string (00.08) (dv)
   10/25/06 removed debugging code for cmd_cpy, mfs_load tests
            use sn_flush to block on putC routines (testing for serputn bug)
            set version string (00.09) (dv)
   10/25/06 revert sio_used to *not* use serhit1, since serhit1 doesn't
            seem to work as advertised. instead, calculate number of characters
            in the input buffer using head and tail indices.
   10/28/06 changed prn command to try to handle buggy flow control in the
            printer controller board. it seems that xon is sent, but garbage is
            sent instead of xoff. also, on power up the printer emits 0x6b as
            status to indicate that the printer is ready to print.
            set version string (00.11) (dv)
   11/06/06 removed flow control work around -- printer flow control seems
            to work. fixed size error in printer output buffer size.
            set version string (00.12) (dv)
   11/08/06 added prn to sio passthru for testing.
            set version string (00.13) (dv)
   11/09/06 reworked flow control in prn command. added delay before sending
            characters to printer after xon is received.
            set version string (00.14) (dv)
   12/18/06 changed to support up to 9999 event files, by adding another
            level of sub-directories number 0..9 which each hold up to 1000
            events. added ENABLE_MANYMANY_EVENTS switch to include the code.
            set version string (00.15) (dv)
   02/01/07 changed sio_shutdown to wait on buffer (drain) before closing.
            enabled printer to sio passthru option.
            set version string (00.16) (dv)
   02/19/07 changed MANYMANY events to use 100 files per directory
            set version string (00.17) (dv)
   07/20/08 added CLK and MSG commands (dv)
   07/20/08 disabled printer interface (dv)
   08/16/08 added parsing for special files (i.e. user_act.dat and
            fact_act.dat, etc. files (00.18) (dv)
   11/01/08 reworked to use multitasking safe disk subsystem (00.19) (dv)
   11/02/08 reworked to be multitasking version only (00.20) (dv)
   11/07/08 clean up, some bug fixes (msg, cpy, put) (00.21) (dv)
   11/11/08 clean up, some bug fixes (cmd_put, mfs_save) (00.22) (dv)
   11/20/08 changed YIELD() from taskYIELD to vTaskDelay(10) (00.24) (dv)
   12/01/08 changed YIELD() to nop (00.25) (dv)
   12/01/08 look for and flush "* " in command test to avoid error feedback
   02/01/09 added mapping for serial ports
   02/06/09 fix command request handling, real delay for YIELD() (00.27) (dv)
   10/07/10 add debug info to error messages in cmd_put (dv)
   10/08/10 remove YIELD() in sio_get_fcte (dv)
   10/08/10 change serial buffer size from 8192 to 25000 (dv)
*/

/** ---------------------------------------------------------------------
*** tern/3000PR INTERFACE DESCRIPTION
***

Use tern serial port 0 to connect to the printer.
Use tern serial port 0 (rts0) to control printer power.
Use tern serial port 1 to connect to the seismograph.

*** ---------------------------------------------------------------------
HARDWARE (newlife)

TERN                   SEISMOGRAPH
RXD1 (J5-6)  RXE  <--  TX
TXD1 (J5-4)  TXE  -->  RX
(??)         DSR  <--  DTR
(??)         DTR  -->  DSR

{ printer is disabled }

*** ---------------------------------------------------------------------
HARDWARE (5500)

TERN                   SEISMOGRAPH
RXD1 (J5-6)  RXE  <--  TX
TXD1 (J5-4)  TXE  -->  RX
(power control)   <--   DTR // (not a tern i/o)

TERN                   PRINTER
RXD0 (J5-5)  RXF  <--  TX
TXD0 (J5-3)  TXF  -->  RX
RTS0 (J2-23) PWR  -->  PWR (power control)

*** ---------------------------------------------------------------------
COMMANDS

***!***
VER
    returns firmware version information
returns:
    . <firmware version information>

***!***
INF
    returns protocol version information
returns:
    . V100 100%   ; protocol version and free space percentage

***!***
PRN ON
    apply power to the printer
returns:
    . OK

***!***
PRN OFF
    removes power from the printer
returns:
    . OK

***!***
PRN JOB
    returns the current status of the printer
returns:
    . IDLE        ; printer is ready to print
    . BUSY        ; printer is busy or the print queue is not empty
    * ERROR       ; printer is unavailable, likely off

***!***
PRN
    (followed by FCTE formatted data)
    sends data to the printer
returns:
    . OK          ; data accepted for printing
    * OVERRUN     ; print queue overflow
    * ERROR       ; printer is unavailable, likely off

***!***
PUT CFG           ; save factory setup file to card
PUT SET           ; save user settings file to card
PUT n
PUT
    (followed by FCTE formatted data)
    save an event to the memory card
returns:
    . OK          ; data accepted for printing
    * OVERRUN     ; disk buffer overflow
    * DISK_FULL   ; no room on disk
    * NO_MEDIA    ; missing disk
    * ERROR       ; disk write error

***!***
LST n m
    lists the files stored on the memory card
returns:
    ; id- ty date---- time---- size
    . 999 99 99/99/99 99:99:99 9999
    . 999 99 99/99/99 99:99:99 9999
    . 999 99 99/99/99 99:99:99 9999
    ...
    . DONE        ; no more directory items
    * NO_MEDIA    ; missing disk
    * ERROR       ; disk read error

***!***
MSG
    get the network computer's state variable. the
    returned state variable is the hex representation
    of the unsigned long state variable.
returns:
    . 1234ABCD    ; hex value of the state variable

***!***
MSG 1234ABCD
    sends the 3000's state variable. the argument is the
    hex representation of the 3000's unsigned long state
    variable.
returns:
    . OK          ; success
    * ERROR       ; error processing command

***!***
CLK
    return the real time clock value
returns:
    . MO/DY/YR HH:MM:SS

***!***
CLR FORMAT
    format the memory card
returns:
    . OK          ; disk format complete
    * NO_MEDIA    ; missing disk
    * ERROR       ; disk format error

***!***
CLR DELETE
    delete all plain files in the data directory
returns:
    . OK          ; all r/w files have been deleted
    * NO_MEDIA    ; missing disk
    * ERROR       ; not all r/w files could be deleted

***!***
CLR n m
    delete events from the memory card
returns:
    . OK          ; delete successful
    * NO_MEDIA    ; missing disk
    * ERROR       ; delete error

***!***
CPY CFG           ; copy factory setup file from card
CPY SET           ; copy user settings file from card
CPY n
CPY n m
    copy an event from the memory card
returns:
    . (data)      ; data is sent in FCTE format
    * NO_MEDIA    ; missing disk
    * ERROR       ; disk read error

***!***
FFS
    fix file system. performs file system checks
returns:
    . OK          ; no problems found, disk is ready
    . REPAIRED    ; disk problems fixed, disk is ready
    * FAILED      ; unable to use disk (maybe write protected)
    * NO_MEDIA    ; missing disk
    * ERROR       ; ffs command error

***!***
<unknown command>
    any unknown or unrecognized command
returns:
    * HUH

*** ---------------------------------------------------------------------
FCTE format

Transmitter:

The transmitter escapes the tilde character (~), replacing it with the
sequence tilde exclamation (~!), so that the sequence tilde tilde (~~)
is never part of the normal data stream. The tilde tilde (~~) sequence
is used to signal an end of file (EOF) condition.

Receiver:

The receiver may send the ^S character to the transmitter to suspend
transmission until the transmitter receives the ^Q character to resume
operation. The receiver may also send the ^X character to signal to the
transmitter to abort its transmission.

*** ---------------------------------------------------------------------
FILE SYSTEM

Events are stored on the memory card in the path specified as

    \DATA\<nnn>.g3k

where

    <nnn> is the event sequence number

Care must be taken to ensure that the sequence number is valid. Problems can
arise if a file is deleted, causing a gap in the sequence. On startup, the
rabbit should perform a consistency check on the sequence number, possibly
renaming (renumbering) the existing data files. During normal operation, the
delete function should be restricted to delete all or the deletion of the file
with the highest sequence number.

The directory, DATA, should be automatically created as needed.

IMPLEMENTATION NOTE:
The TERN does NOT seem to have a way to format a compact flash card.
It's also not clear if subdirectories are completely supported.

*/

#define ASSERT(text, assertion)  if (! (assertion)) assert(text)
#define FATAL(text)  assert(text)

#define MYSPRINTF  sprintf
#define MYSSCANF   sscanf

#define DELAY()   vTaskDelay(3*JIFFIES_PER_SECOND)
#define YIELD()   vTaskDelay(JIFFIES_PER_SECOND/5)
#define LOOPTMO   (3*5)  /* loop time in YIELD() units (1/5s) */

#define MYMALLOC  malloc
#define MYFREE    free

// real-time clock

void RTC_read(struct tm *t)
{
 time_t tim;
 struct tm *tm;

    time(&tim);
    tm = localtime(&tim);
    t->tm_sec = tm->tm_sec;
    t->tm_min = tm->tm_min;
    t->tm_hour = tm->tm_hour;
    t->tm_mday = tm->tm_mday;
    t->tm_mon = tm->tm_mon;
    t->tm_year = tm->tm_year;
    t->tm_wday = tm->tm_wday;
    t->tm_yday = tm->tm_yday;
    t->tm_isdst = tm->tm_isdst;
    return;
}


// ---------------------------------------------------------------------
// SERIAL PORT CONFIGURATION AND MEMORY

#ifdef USE_DYNAMIC_MEMORY_BUFFER
#define ISIZE 31000
#define OSIZE 256
#else
//#define ISIZE  8192
#define ISIZE  23000
#define OSIZE  128
#endif

#define PSIZE  4


// ---------------------------------------------------------------------

// wait on serial output before sending more
#define USE_putC_WORKAROUND

// doesn't seem to be needed
// #define USE_PRINTER_BUG_WORKAROUND

// not sure why, but we seem to need this
#define ENABLE_PRN_TO_SIO_PASSTHRU

// ---------------------------------------------------------------------
// FILE BUFFER MEMORY

#ifdef USE_DYNAMIC_MEMORY
#define gMEMORY_DECL(x)  static char * gMEMORY
#define gMEMORY_INIT(x)  gMEMORY = MYMALLOC(x)
#else
#define gMEMORY_INIT(x)
#define gMEMORY_DECL(x)  static char gMEMORY[x]
#endif

#define kUNKNOWN  -9999
#define kMEMSIZE  31000

#define MN 16
#define MX 30992

gMEMORY_DECL(kMEMSIZE);

// we define 0x8000 in this way to suppress some comparison warnings
#define k0x8000 -32768

// ---------------------------------------------------------------------
// FILE SYSTEM (DISK) INTERFACE

#define SET_FILE_ACT  DS_DEVSET_A
#define SET_FILE_REQ  DS_DEVSET_Q
#define CFG_FILE_ACT  DS_DEVCFG_A
#define CFG_FILE_REQ  DS_DEVCFG_Q
#define NEXTFILE      DS_NEXTFILE

#define NTOHS(x)  ((int)((((x)<<8)&0xFF00)|(((x)>>8)&0x00FF)))

static int writeDebugF1(const char *str);

static int mfs_mediadetect (void)
{
	return(0);
//D!    return disk_mediadetect ();
}

static int mfs_initialize (void)
{
    return 0;
}

static int mfs_shutdown (void)
{
    return 0;
}

static int mfs_repair (int flags)
{
    if (flags & 1) {
        // format is not supported on the tern, so
        // we have no unconditional format command
    }

    return 0;
}

static int mfs_files (void)
{
    // ###FIXME!###
    // we're really looking for the last (highest) record
    // number on disk. it should correspond to the number
    // of data files, but actually finding the last record
    // would be a more robust approach.
    //
    // return disk_count ();

    int e;

    e = 0;      // ~!~!~!~!~!~!~!~!~!~! Delete ME!!!!!!!!
//D!    e = disk_next ();
    if (e > 0) return e - 1;

//D!    e = disk_next ();
    if (e > 0) return e - 1;

//D!    e = disk_next ();
    if (e > 0) return e - 1;

#if 0
    // hopefully, we never get here. we don't handle
    // this error condition too gracefully elsewhere.
    FATAL("mfs_files: disk_next failed");
#else
    // mark the disk error in net task AV flags
    network_SetRecAV (0xffff);
#endif

    return 0;
}

static int mfs_load (char * m, int f, int n)
{
	return(1);
//D!    return disk_fetch (f, n, (unsigned char *) m);
}

static int mfs_head (char * m, int i)
{
    int e;

    * ((int *) (m + 0x200)) = i;

//D!    e = disk_fetch (i, 0x200, (unsigned char *) m);
    if (e == DS_PARTIALREAD)
        e = 0; // DS_OK

    if (e > 0)
        e = 0; // DS_OK

#if 0
    // mark the disk error in net task AV flags
    if (e < 0) network_SetRecAV (0xffff);
#endif

    return e;
}

static int mfs_save (char * m, int f, int n)
{
    int e;
#ifdef FOO
    e = ((f == SET_FILE_ACT) ||
         (f == SET_FILE_REQ) ||
         (f == CFG_FILE_ACT) ||
         (f == CFG_FILE_REQ))
        ? disk_store_insitu (f, n, (unsigned char *) m)
        : disk_store (f, n, (unsigned char *) m);
    // mark the disk error in net task AV flags
    if (e < 0) network_SetRecAV (0xffff);
#endif
    return e;
}

static int mfs_delete (int i)
{
//D!    if (i >  0) return disk_delete (i, i);
//D!    if (i == 0) return disk_delete (0, 0x7FF0);
    return -1;
}

static int mfs_space (void)
{
    // ###FIXME!###
    // since for our purposes it's a big flash card, we can be lazy
    // (and quick) and always show that there's space (99% free).
    return 99;
}


static int sio_get_fcte (char * s)
{
    int n, c, u, t;

    t = LOOPTMO;
    u = 0;
    n = 0;

    while (n < MX) {
        c = getC(BRIDGE_PORT, 1111);    // ~! Timeout is wrong!

        if (c < 0) {
            // give up on loop tmo
            if (--t < 0) return -1;
            YIELD();
//*!*       sleep(100);
            continue;
        }

        t = LOOPTMO;

        if (u) {
            u = 0;
            if (c == '~') {
                n--;
                break;
            }
            if (c == '!') {
                continue;
            }
        }
        if (c == '~') u = 1;
        * s++ = c;
        n++;
    }
    return n;
}

static void sio_put_fcte (char * s, int n)
{
    int c, u = 0;

    while (n > 0) {
        c = getC(BRIDGE_PORT, 1111);

        if (c == 0x18)
            // ^X abort
            return;

        if (c >= 0)
            // ^S suspend
            u = (c == 0x13);

        if (u)
            continue;

        putC(BRIDGE_PORT, *s);
        if (* s == '~')
            putC(BRIDGE_PORT, '!');

        s++;
        n--;
    }

    putC(BRIDGE_PORT, '~');
    putC(BRIDGE_PORT, '~');
}


// ---------------------------------------------------------------------
// PRINTER PORT INTERFACE

#ifndef NO_PRINTER_INTERFACE

static int prn_ison (void)
{
    return prn_rts_shadow;
}

static void prn_ctrl (int e)
{
    prn_rts_shadow = (e != 0);
    PRNrts ((char) prn_rts_shadow);
}

static int prn_getc (void)
{
    // returns -1 if no input
    if (PRNserhit (& prnctl) > 0)
        return PRNgetser (& prnctl);
    return -1;
}

#pragma argsused(c)

static void prn_putC (int c)
{
#ifdef USE_putC_WORKAROUND
    PRNflush (& prnctl);
#endif
    /* tight loop here, maybe we can do something
    to reduce power consumption (low power wait) */

    for (;;) {
        if (PRNputSer (c, & prnctl)) break;
        YIELD();
    }
}

static int prn_shutdown (void)
{
    PRNflush (& prnctl);
    PRNclose (& prnctl);
    prn_ctrl (0);
    return 0;
}

static int prn_initialize (void)
{
    prn_ctrl (1);
    // returns true if error
    PRNinit (BAUD19200, prninp, ISIZE, prnout, PSIZE, & prnctl);
    return 0;
}

#else

static int prn_shutdown (void)
{
    return 0;
}

#endif

// ---------------------------------------------------------------------
// PROTOCOL INTERFACE (COMMANDS)

static int mystrcmp (char * inp, char * cmp)
{
    while (* cmp)
        if (toupper(* inp++) != * cmp++)
            return 1;
    return 0;
}

static int isspecial (char * m, int * r)
{
    int c, s;

    s = 0;
    if (mystrcmp (m, "7FF") == 0) {
        c = toupper (* (m + 3));
        if ((c >= '0') && (c <= '9')) s = 0x7ff0 + ((c + 0) & 0xf);
        if ((c >= 'A') && (c <= 'F')) s = 0x7ff0 + ((c + 9) & 0xf);
    }
    if (s) {
        * r = s;
        return 1;
    }
    return 0;
}

static char * workaround_1 (char * m, int * r)
{
    char s;

    s = 0;

    for ( ; * m; m++) if (* m == 0x20) break;
    for ( ; * m; m++) if (* m != 0x20) break;

    if (isspecial (m, r))
        return m + 4;

    if (* m == '-') { s = 1; m++; }
    if (* m == '+') { s = 0; m++; }

    if (isdigit (* m)) * r = 0;

    for ( ; isdigit (* m); m++)
        * r = 10 * * r + (* m & 0xF);

    if (s) * r = - * r;

    return m;
}

static void workaround_2 (char * m, int * r, int * s)
{
    workaround_1 (workaround_1 (m, r), s);
}

static int special_parse (char * m, int * r, int * s, int e, int t)
{
    int z, x;

    * r = k0x8000;
    * s = k0x8000;

    /* sscanf (m, "%*s %d %d", r, s); */
    workaround_2 (m, r, s);

    if (e == 0) x = * s;

    if (* s == k0x8000) * s = * r;

    if ((* r == e) && (* s == e)) {
        * r = 1;
        * s = 0x7FFF;
    }

    z = t + 1;

    if (* r < 0) * r += z;
    if (* s < 0) * s += z;

    z--;

    if (e == 0x7FFF) {
        if ((* r < 1) || (* r > z) || (* s < 1) || (* s > z)) {
            * r = 1;
            * s = z;
            return 1;
        }
    }

    if (* r < 1) * r = 1;
    if (* r > z) * r = z;
    if (* s < 1) * s = 1;
    if (* s > z) * s = z;

    if (e == 0) * s = x;

    return 0;
}

// ---------------------------------------------------------------------

#if 1
#define PUT_SET_FILE  SET_FILE_ACT
#else
#define PUT_SET_FILE  SET_FILE_REQ
#endif

static int put_get_file_id (char * m)
{
    int r = k0x8000;
    int s = k0x8000;
    if (mystrcmp (m, "PUT SET") == 0) return PUT_SET_FILE;
    if (mystrcmp (m, "PUT CFG") == 0) return CFG_FILE_ACT;
    if (mystrcmp (m, "CPY SET") == 0) return SET_FILE_REQ;
    if (mystrcmp (m, "CPY CFG") == 0) return CFG_FILE_REQ;
    workaround_2 (m, & r, & s);
    return r;
}

// ---------------------------------------------------------------------

static int writeDebugF1(const char *str)
{
    char s[160];
    time_t t;
    struct tm *tm;

    time(&t);
    tm = localtime(&t);

#define bcd(x) (((x) & 0xf) | 0x30)

    sprintf(s, "20%c%c-%c%c-%c%cT%c%c:%c%c:%c%cZ %1.138s\n",
             bcd(tm->tm_year), bcd(tm->tm_year),
             bcd(tm->tm_mon),  bcd(tm->tm_mon),
             bcd(tm->tm_mday), bcd(tm->tm_mday),
             bcd(tm->tm_hour), bcd(tm->tm_hour),
             bcd(tm->tm_min),  bcd(tm->tm_min),
             bcd(tm->tm_sec),  bcd(tm->tm_sec),
             str);

    s[159] = 0;

//D!    disk_append (DS_GENERAL, strlen(s), (unsigned char *) s);
    return 0;
}


#define DEBUG_CMD_PUT  1

#define SIZE()  NTOHS(* ((int *) gMEMORY))

// ***bt - 01/02/2018
// Added logging to debug file F1 (writeDebugF1())

static void cmd_put (char * m)
{
    int r, s, x;
    char temp[30];

#if DEBUG_CMD_PUT
    char * p;
#endif
	// Log filename for debug
	sprintf(temp, "%s", m);
	writeDebugF1(temp);

	if(mfs_mediadetect ())
	{
        	p = "* NO_MEDIA";
		putS(BRIDGE_PORT, p);
		goto __exit;
//        return;
	}

	if(mfs_initialize ())
	{
        	p = "* ERROR";
		putS(BRIDGE_PORT, p);
		goto __exit;
//        return;
	}

    serialFlush(BRIDGE_PORT);

    /* ^Q, go! */
    putC(BRIDGE_PORT, 0x11);

    s = sio_get_fcte(gMEMORY);
    if (s == 0) s = -1;

#if DEBUG_CMD_PUT
    p = "* ERROR (fcte)";
    if (! (s > 0)) goto __exit;
#endif

#if DEBUG_CMD_PUT
    p = "* ERROR (drop)";
    if (s < SIZE()) goto __exit;
#endif

    // expect/require first two bytes to be size
    if (s > 0) {
        if (! (s < SIZE())) {

            r = put_get_file_id (m);
            if (r < 0x7ff0)
                if (special_parse (m, & r, & s, 0x7FFF, mfs_files ()))
                    r = s + 1;

            x = s = SIZE();
            if (s < MN) s = -98;
            if (s > MX) s = -99;

#if DEBUG_CMD_PUT
	    sprintf(temp, "* ERROR(size)%04X-%04X", s, x);
            p = temp;
            if (! (s > 0)) goto __exit;
#endif
            if (s > 0)
                s = mfs_save (gMEMORY, r, s);

#if DEBUG_CMD_PUT
            p = "* ERROR (disk)";
            if (s != 0) goto __exit;
#endif
            if ((s == 0) && (r <= NEXTFILE))
                network_BumpNN ();
        }
    }

#if DEBUG_CMD_PUT
    p = ". OK";
__exit:
    writeDebugF1(p);
    putS(BRIDGE_PORT, p);
#else
    putS(BRIDGE_PORT, s ? "* ERROR" : ". OK");
#endif

    mfs_shutdown ();
}

// ---------------------------------------------------------------------

static void cmd_cpy (char * m)
{
    int r, s;

    if (mfs_mediadetect ()) {
        putS(BRIDGE_PORT, "* NO_MEDIA");
        return;
    }

    if (mfs_initialize ()) {
        putS(BRIDGE_PORT, "* ERROR");
        return;
    }

    r = put_get_file_id (m);
    if (r < 0x7ff0)
        special_parse (m, & r, & s, 0, mfs_files ());

    // MX + 2 here to avoid possible DS_PARTIALREAD
    s = mfs_load (gMEMORY, r, MX + 2);

    if (! (s > 0)) {
        putS(BRIDGE_PORT, "* ERROR");
    }
    else {
        putC(BRIDGE_PORT, '.');
        putC(BRIDGE_PORT, ' ');
        sio_put_fcte(gMEMORY, s);
        putC(BRIDGE_PORT, '\r');
    }

    mfs_shutdown ();
}

// ---------------------------------------------------------------------

static void cmd_ver (char * m)
{
    putS(BRIDGE_PORT, ". " VERSION_STRING);
}

// ---------------------------------------------------------------------

static void cmd_inf (char * m)
{
    if (mfs_mediadetect ()) {
        putS(BRIDGE_PORT, "* V100 NO_MEDIA");
        return;
    }

    if (mfs_initialize ()) {
        putS(BRIDGE_PORT, "* V100 DISK_ERROR");
        return;
    }

    MYSPRINTF (gMEMORY, ". V100 %d%% %d", mfs_space (), mfs_files ());
    putS(BRIDGE_PORT, gMEMORY);

    mfs_shutdown ();
}

// ---------------------------------------------------------------------

#ifndef NO_PRINTER_INTERFACE

static void cmd_prn (char * m)
{
    int c, u, n;

    if (mystrcmp (m, "PRN ON") == 0) {
        prn_initialize ();
        putS(BRIDGE_PORT, ". OK");
        return;
    }

    if (mystrcmp (m, "PRN OFF") == 0) {
        prn_shutdown ();
        putS(BRIDGE_PORT, ". OK");
        return;
    }

    if (! prn_ison ()) {
        putS(BRIDGE_PORT,"* ERROR");
        return;
    }

    if (mystrcmp (m, "PRN JOB") == 0) {
        putS(BRIDGE_PORT, ". IDLE");
        return;
    }

    // flow-controlled buffered printer pass thru

    u = 0;

    for (;;) {
        // throttle serial port
        n = sio_used ();
        if (u & 8)  {
            if (n < ((ISIZE*1)/8))  {
                putChar(BRIDGE_PORT, 0x11);
                u &= ~8;
            }
        }
        else {
            if (n > ((ISIZE*3)/4)) {
                putChar(BRIDGE_PORT, 0x13);
                u |=  8;
            }
        }

        // xon/xoff -- 0x13 => xoff, all others => xon
/////////~!~!~!        c = prn_getc ();
        if (! (c < 0)) u &= ~2;
        if (c == 0x13) u |=  2;

#ifdef ENABLE_PRN_TO_SIO_PASSTHRU
        if (! (c < 0)) sio_putC (c);
#endif

#ifdef USE_PRINTER_BUG_WORKAROUND
        // give the printer a chance to catch up
        if (c == 0x11) sleep(10);
#endif

        if (u & 2) continue;

        // ser to prn protocol processing
        c = sio_getc ();
        if (c < 0) continue;

        if (u & 1) {
            u &= ~1;

            if (c == '~')
              break;

            prn_putC('~');

            if (c == '!')
              continue;
        }
        else {
            if (c == '~')  {
                u |= 1;
                continue;
            }
        }

        prn_putC (c);
    }

    sio_putS (". OK");
}

#else

static void cmd_prn (char * m)
{
    putS (BRIDGE_PORT, "* ERROR");
}

#endif

// ---------------------------------------------------------------------

static void cmd_clr (char * m)
{
    int r, s;

    if (mfs_mediadetect ()) {
        putS(BRIDGE_PORT, "* NO_MEDIA");
        return;
    }

    if (mystrcmp (m, "CLR FORMAT") == 0) {
        r = mfs_repair (1);
        putS(BRIDGE_PORT, r ? "* ERROR" : ". OK");
        return;
    }

    if (mfs_initialize ()) {
        putS(BRIDGE_PORT, "* ERROR");
        return;
    }

    if (mystrcmp (m, "CLR DELETE") == 0) {
        if (mfs_delete (0))
            goto eexit;
        goto oexit;
    }

    if (special_parse (m, & r, & s, 0x7FFF, mfs_files ()))
        goto eexit;

    if (r > 0) {
        while (r <  s) if (mfs_delete (r++)) goto eexit;
        while (s <= r) if (mfs_delete (s++)) goto eexit;
    }

oexit:
    putS(BRIDGE_PORT, ". OK");
    mfs_shutdown ();
    return;

eexit:
    putS(BRIDGE_PORT, "* ERROR");
    mfs_shutdown ();
}

// ---------------------------------------------------------------------

#define BY(m) ((unsigned char) (* (gMEMORY + m)))

static char * format_item (int e)
{
    // ; id- ty date---- time---- -size
    // . 999 99 99/99/99 99:99:99 99999

    if (e)
        MYSPRINTF (gMEMORY + 0x300,
                 "* %04.4d ERROR",
                 * ((int *) (gMEMORY + 0x200)));
    else
        MYSPRINTF (gMEMORY + 0x300,
                 ". %04.4d %02X %02X/%02X/%02X %02X:%02X:%02X %-d",
                 * ((int *) (gMEMORY + 0x200)),
                 BY(12),
                 BY(5), BY(6), BY(4), BY(7), BY(8), BY(9),
                 NTOHS (* ((int *) gMEMORY)));

    return gMEMORY + 0x300;
}

static void cmd_lst (char * m)
{
    int r, s;

    if (mfs_mediadetect ()) {
        putS(BRIDGE_PORT, "* NO_MEDIA");
        return;
    }

    if (mfs_initialize ()) {
        putS(BRIDGE_PORT, "* ERROR");
        return;
    }

    special_parse (m, & r, & s, 0x8000, mfs_files ());
    if (r > 0) {
        while (r <  s) putS(BRIDGE_PORT, format_item (mfs_head (gMEMORY, r++)));
        while (s <= r) putS(BRIDGE_PORT, format_item (mfs_head (gMEMORY, s++)));
    }
    putS(BRIDGE_PORT, ". DONE");
    mfs_shutdown ();
}

// ---------------------------------------------------------------------

static void cmd_ffs (char * m)
{
    if (mfs_mediadetect ()) {
        putS(BRIDGE_PORT, "* NO_MEDIA");
        return;
    }

    if (mfs_repair (0)) {
        putS(BRIDGE_PORT, "* FAILED");
        return;
    }

    putS(BRIDGE_PORT, ". OK");
}

// ---------------------------------------------------------------------

static void cmd_clk (char * m)
{
    char s [24];
    struct tm tm;

    RTC_read(&tm);

    MYSPRINTF (s, ". %02d/%02d/%02d %02d:%02d:%02d",
             tm.tm_mon,
             tm.tm_mday,
             tm.tm_year,
             tm.tm_hour,
             tm.tm_min,
             tm.tm_sec);
    putS(BRIDGE_PORT, s);
}

// ---------------------------------------------------------------------
// MESSAGE LOG AND COMMAND REQUEST
//

#define  kWAIT (JIFFIES_PER_SECOND * 8)

typedef struct {
    ulong info;
    ulong time;
} STATUS;

static STATUS msg = { 0, 0 };
static void * semaphore = NULL;
static uint req = 0;

static void log_unpend (ulong u) {

    if (! (u & 0x80000000L)) {
        switch (u & 0xff) {
        case 0x97:
        case 0x17:
        case 0x16:
        case 0x15:
        case 0x14:
            supervisor_CLR (kBRIDGE_PEND);
        }
    }
}

static char * log_worthy (ulong u, char * s)
{
    if (u & 0x80000000L) {
        switch (u & 0xff) {
        case 0xfe: return s;
        case 0xfd: return "IF: USE_INI";
        case 0xfc: return "IF: SHUTDOWN";
        case 0xfb: return "IF: POWER_ON";
        case 0xfa: return "IF: SLEEP";
        case 0xf9: return "IF: AWAKE";
        case 0xf8: return "IF: RESET";
        case 0xf7: return "IF: FAULT";
        case 0xf6: return "IF: LOW POWER";
        }
        return NULL;
    }

    switch (u & 0xff) {
    case 0x0a:
    case 0x09:
    case 0x89:
    case 0xc9:
    case 0x08:
    case 0x88:
    case 0xc8: return s;
    case 0x97: return "ERR";
    case 0x17: return "SLP";
    case 0x16: return "RUN";
    case 0x15: return "CMD";
    case 0x14: return "PRG";
    }
    return NULL;
}

#define BCD(x) (((x) & 0xf) | 0x30)

#ifdef USE_INSITU_LOGFILE

#define USE_PREALLOCATED_LOGBUF
#ifdef  USE_PREALLOCATED_LOGBUF
static char * gLOGBUF;
#endif

static void log (struct tm *t, ulong u, char * s)
{
#ifdef FFFFFFFFFFOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    int e;
    char * p;
#ifndef USE_PREALLOCATED_LOGBUF
    char * M;
#endif

    s = log_worthy (u, s);
    if (s) {

#ifdef USE_PREALLOCATED_LOGBUF
#define M  gLOGBUF
#else
        M = MYMALLOC(kMAXLOG);
#endif

        if (M) {

            p = M;

//D!            e = disk_fetch (DS_MESSAGES, kMAXLOG-1, (uchar *) p);
            if (e < 0) * p = 0; else * (p + e) = 0;

            if (e < 0) {
                /* erase or create the file as necessary if error */
//D!                e = disk_store_insitu (DS_MESSAGES, kMAXLOG, NULL);
            }

            /* find the end */
            while (* p) p++;

            if ((p - M) > (kMAXLOG - 0x100)) {
                p = M;
                e = MYSPRINTF (p, "\t\t<TRUNCATED/>\n");
                if (e > 0) p += e;
            }

            if (e < 0) {
                e = MYSPRINTF (p, "\t\t<ERROR N=\"%d\"/>\n", e);
                if (e > 0) p += e;
            }

            e = MYSPRINTF (p,"\t\t<msg t=\"%04d-%02d-%02dT%02d:%02d:%02dZ\">%1.158s</msg>\n",
                           t->tm_year,
                           t->tm_mon,
                           t->tm_mday,
                           t->tm_hour,
                           t->tm_min,
                           t->tm_sec,
                           s);

            if (e > 0) p += e;

            * p = 0;

            e = (int) (p - M);
//D!            e = disk_store_insitu (DS_MESSAGES, (e + 1), (uchar *) M);
            /* problems here should cause some kind of assertion */

#ifdef USE_PREALLOCATED_LOGBUF
#undef M
#else
            MYFREE(M);
#endif
            if (e < 0) {
                // mark the disk error in net task AV flags
                network_SetRecAV (0xffff);
            }
        }
    }
#endif
    return;
}

static int mystrlen (const char * p)
{
    int n = 1;
    if (p) {
        while (* p++) n++;
        return n;
    }
    return 0;
}

static void logfix (int n, int sz, char * mp)
{
    int e;

    // log couldn't be read by the heartbeat code
    if (n < 0) n = 0;

//D!    e = disk_fetch (DS_MESSAGES, sz, (uchar *) mp);
    if (! (e < 0)) {
        if (n > 0) n--;
        if (e > 0) e--;
        * (mp + e) = 0;
        if (e < n) {
            // may happen on truncate
            e = mystrlen (mp);
//D!            disk_store_insitu (DS_MESSAGES, e, (uchar *) mp);
        }
        else {
            e = mystrlen (mp + n);
//D!            disk_store_insitu (DS_MESSAGES, e, (uchar *) mp + n);
        }
    }
    else {
        // unable to read the log file, try to re-init the log
//D!        disk_store_insitu (DS_MESSAGES, kMAXLOG, NULL);
    }
}

#else

static void log (struct TM *t, ulong u, char *s)
{
    int n;
    char M [255];

    s = log_worthy (u, s);
    if (s) {

//D!        n = disk_fsize (DS_MESSAGES);

        if (n < 0) {
//D!            n = disk_fsize (DS_MESSAGES);
        }

        if (n > kMAXLOG) {
//D!            disk_store (DS_MESSAGES, 15, (uchar *) "\t\t<TRUNCATED/>\n");
        }

        /* n = -100 (end of directory) does not seem to be a legitimate
           error. behind the scenes, it looks like fs_fopen incorrectly
           returns an "end of directory" error when opening small files
           as read only. UGLY WORK-AROUND: we ignore this error here */

        /* n = -9 (disk integrity) also does not seem to be a legitimate
           error. behind the scenes, it looks like fs_change_dir incorrectly
           returns something other than ok when changing to the DATA folder.
           UGLY WORK-AROUND: we will ignore this error here, as well */

        if ((n < 0) && (n != -100) && (n != -9)) {
            n = MYSPRINTF (M, "\t\t<ERROR N=\"%d\"/>\n", n);
            if (n > 0) {
                /* since we detected an error here, it's not clear that
                   we'll be able to write to the disk, but we will try
                   anyway. while erasing and writing a new file might
                   fix more errors, append data so that we can try to
                   protected any existing log messages */
                // disk_store (DS_MESSAGES, n, (uchar *) M);
//D!                disk_append (DS_MESSAGES, n, (uchar *) M);
            }
        }

            e = MYSPRINTF (p,"\t\t<msg t=\"%04d-%02d-%02dT%02d:%02d:%02dZ\">%1.158s</msg>\n",
                           t->tm_year),
                           t->tm_mon,
                           t->tm_mday,
                           t->tm_hour,
                           t->tm_min,
                           t->tm_sec),
                           s);

        if (n > 0) {
//D            disk_append (DS_MESSAGES, n, (uchar *) M);
        }
    }
}

static void logfix (int n, int sz, char * mp)
{
    int e;

//D!    e = disk_fetch (DS_MESSAGES, sz, (uchar *) mp);
    if (! (e < n)) {
//D!        disk_store (DS_MESSAGES, e - n, (uchar *) mp + n);
    }
}

#endif


void bridge_logadd (ulong u, char * s)
{
    struct tm t;

    if (semaphore)
    {
        if (xSemaphoreTake (semaphore, kWAIT) == pdTRUE) {
            RTC_read(&t);
            log (& t, u, s ? s : "");
            log_unpend (u);
            xSemaphoreGive (semaphore);
        }
    }
}

void bridge_logfix (int n, int sz, char * mp)
{
    if (semaphore) {
        if (xSemaphoreTake (semaphore, kWAIT) == pdTRUE) {
            logfix (n, sz, mp);
            xSemaphoreGive (semaphore);
        }
    }
}

ulong bridge_status (STATUS * s)
{
    if (semaphore) {
        if (xSemaphoreTake (semaphore, kWAIT) == pdTRUE) {
            s->time = msg.time;
            s->info = msg.info;
            xSemaphoreGive (semaphore);
            return s->info;
        }
    }
    return 0;
}

static void cmd_msg (char * m)
{
    struct tm t;
    long u;
    int n;

    if (* (m + 3)) {
        if (MYSSCANF (m, "%*s %lx%n", & u, & n) == 1) {
            putS(BRIDGE_PORT, ". OK");
            if (semaphore) {
                if (xSemaphoreTake (semaphore, kWAIT) == pdTRUE) {
                    RTC_read(& t);
                    msg.time = xTaskGetTickCount();
                    msg.info = u;
                    log (& t, u, m + n + (* (m + n) != 0));
                    xSemaphoreGive (semaphore);
                }
            }
            network_SetRecAV ((unsigned short) u);
        }
        else {
            putS (BRIDGE_PORT, "* ERROR");
        }
    }
    else {
        MYSPRINTF ((char *) & t, ". %x", req);
        putS(BRIDGE_PORT, (char *) & t);
        req = 0;
    }
}

// request interface

unsigned int bridge_GetREQ (void)
{
    return req;
}

void bridge_SetREQ (unsigned int c)
{
    req = c;
}

// ---------------------------------------------------------------------
// TASK (BRIDGE)

int bridge_init (void)
{
    ASSERT("bridge_init: called twice", semaphore == NULL);

    semaphore = xSemaphoreCreateMutex ();
    gMEMORY_INIT(kMEMSIZE);


    ASSERT("bridge_init: no semaphore", semaphore != NULL);
    ASSERT("bridge_init: memory error", gMEMORY != NULL);

#ifdef USE_PREALLOCATED_LOGBUF
    gLOGBUF = MYMALLOC(kMAXLOG);
    ASSERT("bridge_init: logbuf error", gLOGBUF != NULL);
#endif

#ifdef USE_DYNAMIC_MEMORY_BUFFER
    sioinp = MYMALLOC (ISIZE);
    sioout = MYMALLOC (OSIZE);
    ASSERT("bridge_init: sioinp malloc", sioinp != NULL);
    ASSERT("bridge_init: sioout malloc", sioout != NULL);
    return sioinp && sioout && semaphore && gMEMORY;
#else
    return (gMEMORY!=NULL);
#endif
}

int bridge_is_active (void) {
    // dtr from 3000 is asserted
    return ! getCTS(BRIDGE_PORT);
}


#define REVERTED_RTS

#ifdef REVERTED_RTS
#else
static void maybe_toggle_rts (void)
{
    if (serialCTS(BRIDGE_PORT)) {
        // 3000 serial port is off
        // force ring on 3000 (dtr)
        sleep(1000);
        serialRTS(BRIDGE_PORT, 1);
        sleep(2000);
    }
    serialRTS(BRIDGE_PORT, 0);
}
#endif

//#pragma argsused

void bridge_task (void * parameters)
{
    char * s;
    int loop = 1;

    supervisor_SET (kBRIDGE_BUSY);

    mfs_shutdown ();
    prn_shutdown ();

    serialOpen(BRIDGE_PORT, BAUD19200, 0, 0);
    do {
        LED3_TOGGLE();
        vTaskDelay(100 / portTICK_RATE_MS);

        if (req) {
            serialFlush(BRIDGE_PORT);
            /* allow the transmitter to finish */
            /* (could test the uart directly)  */
            sleep(8);
#ifdef REVERTED_RTS
            setRTS(BRIDGE_PORT, 0);
#else
            maybe_toggle_rts ();
#endif
            putC(BRIDGE_PORT, '!');
        }
        do {
            s = getS(BRIDGE_PORT, 1111);
            if (s && * s) {
                /**/ if (mystrcmp (s, "PUT") == 0) cmd_put (s);
                else if (mystrcmp (s, "MSG") == 0) cmd_msg (s);
                else if (mystrcmp (s, "CLK") == 0) cmd_clk (s);
                else if (mystrcmp (s, "PRN") == 0) cmd_prn (s);
                else if (mystrcmp (s, "LST") == 0) cmd_lst (s);
                else if (mystrcmp (s, "CPY") == 0) cmd_cpy (s);
                else if (mystrcmp (s, "CLR") == 0) cmd_clr (s);
                else if (mystrcmp (s, "INF") == 0) cmd_inf (s);
                else if (mystrcmp (s, "FFS") == 0) cmd_ffs (s);
                else if (mystrcmp (s, "VER") == 0) cmd_ver (s);
                else if (mystrcmp (s, "~~" ) == 0) continue; // no-op
                else if (mystrcmp (s, "* " ) == 0) continue; // no-op
                // else if (mystrcmp (s, "QUI") == 0) loop = 0;
#if 0
                else sio_putS ("* HUH");
#else
                else {
                    putC(BRIDGE_PORT, '*');
                    putC(BRIDGE_PORT, ' ');
                    putC(BRIDGE_PORT, 'H');
                    putC(BRIDGE_PORT, 'U');
                    putC(BRIDGE_PORT, 'H');
                    putC(BRIDGE_PORT, ' ');
                    putS(BRIDGE_PORT, s);
                }
#endif
            }
            break;
        } while (1);
#ifdef REVERTED_RTS
        setRTS(BRIDGE_PORT, 1);
#else
#endif
        if (! req) {
            if (supervisor_suspend_p ()) {
                supervisor_CLR (kBRIDGE_BUSY);
                while (supervisor_suspend_p ())
                    YIELD();
                supervisor_SET (kBRIDGE_BUSY);
            }
        }

        YIELD();
    LED2_TOGGLE();
    } while (loop);
    serialFlush(BRIDGE_PORT);

    /* don't fall off the end here, do something safe */
    for (;;) YIELD();
}
