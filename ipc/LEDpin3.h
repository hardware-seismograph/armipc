/** ###################################################################
**     THIS COMPONENT MODULE IS GENERATED BY THE TOOL. DO NOT MODIFY IT.
**     Filename  : LEDpin3.h
**     Project   : XEP100
**     Processor : MC9S12XEP100CVL
**     Component : BitIO
**     Version   : Component 02.075, Driver 03.14, CPU db: 3.00.036
**     Compiler  : CodeWarrior HCS12X C Compiler
**     Date/Time : 11.02.2012, 11:08
**     Abstract  :
**         This component "BitIO" implements an one-bit input/output.
**         It uses one bit/pin of a port.
**         Note: This component is set to work in Output direction only.
**         Methods of this component are mostly implemented as a macros
**         (if supported by target language and compiler).
**     Settings  :
**         Used pin                    :
**             ----------------------------------------------------
**                Number (on package)  |    Name
**             ----------------------------------------------------
**                       96            |  PA2_ADDR10_IVD10
**             ----------------------------------------------------
**
**         Port name                   : A
**
**         Bit number (in port)        : 2
**         Bit mask of the port        : $0004
**
**         Initial direction           : Output (direction cannot be changed)
**         Initial output value        : 0
**         Initial pull option         : off
**
**         Port data register          : PORTA     [$0000]
**         Port control register       : DDRA      [$0002]
**
**         Optimization for            : speed
**     Contents  :
**         GetVal - bool LEDpin3_GetVal(void);
**         ClrVal - void LEDpin3_ClrVal(void);
**         SetVal - void LEDpin3_SetVal(void);
**         NegVal - void LEDpin3_NegVal(void);
**
**     Copyright : 1997 - 2010 Freescale Semiconductor, Inc. All Rights Reserved.
**     
**     http      : www.freescale.com
**     mail      : support@freescale.com
** ###################################################################*/

#ifndef LEDpin3_H_
#define LEDpin3_H_

/* MODULE LEDpin3. */

  /* Including shared modules, which are used in the whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "Cpu.h"

#pragma CODE_SEG LEDpin3_CODE
/*
** ===================================================================
**     Method      :  LEDpin3_GetVal (component BitIO)
**
**     Description :
**         This method returns an input value.
**           a) direction = Input  : reads the input value from the
**                                   pin and returns it
**           b) direction = Output : returns the last written value
**         Note: This component is set to work in Output direction only.
**     Parameters  : None
**     Returns     :
**         ---             - Input value. Possible values:
**                           FALSE - logical "0" (Low level)
**                           TRUE - logical "1" (High level)

** ===================================================================
*/
#define LEDpin3_GetVal() ( \
    (bool)((getReg8(PORTA) & 0x04U))   /* Return port data */ \
  )

/*
** ===================================================================
**     Method      :  LEDpin3_ClrVal (component BitIO)
**
**     Description :
**         This method clears (sets to zero) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define LEDpin3_ClrVal() ( \
    (void)clrReg8Bits(PORTA, 0x04U)    /* PA2=0x00U */ \
  )

/*
** ===================================================================
**     Method      :  LEDpin3_SetVal (component BitIO)
**
**     Description :
**         This method sets (sets to one) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define LEDpin3_SetVal() ( \
    (void)setReg8Bits(PORTA, 0x04U)    /* PA2=0x01U */ \
  )

/*
** ===================================================================
**     Method      :  LEDpin3_NegVal (component BitIO)
**
**     Description :
**         This method negates (inverts) the output value.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
#define LEDpin3_NegVal() ( \
    (void)invertReg8Bits(PORTA, 0x04U) /* PA2=invert */ \
  )

#pragma CODE_SEG DEFAULT

/* END LEDpin3. */
#endif /* #ifndef __LEDpin3_H_ */
/*
** ###################################################################
**
**     This file was created by Processor Expert 3.02 [04.44]
**     for the Freescale HCS12X series of microcontrollers.
**
** ###################################################################
*/
