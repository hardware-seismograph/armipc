/* TERN.H - multi-tasking safe access to tern libraries

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary
work and also under the United States Copyright Act of 1976, as amended.
Distribution and access are limited only to authorized persons. Disclosure,
reproduction, distribution or use, such as use to prepare derivative works,
may violate federal andn state laws.
*/

/* HISTORY
10-20-08 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifndef __TERN_H__
#define __TERN_H__

#define UINT  unsigned int
#define LONG  unsigned long
#define ULNG  unsigned long
#define FS    struct fs_descrip

extern void TERN_ae_init (void);
extern void __TERN_ae_reset (void);
extern void __TERN_delay_ms (int m);
extern void __TERN_pwr_save_en (int i);
extern void __TERN_pwr_save (void);
extern void __TERN_hitwd (void);

extern void TERN_pio_init (char bit, char mode);
extern void TERN_pio_wr (char bit, char data);
extern UINT TERN_pio_rd (char port);
extern void TERN_led (int on);

extern int __TERN_ee_rd (int a);
extern int __TERN_ee_wr (int a, uchar d);
//extern int TERN_ee_rd (int a);
//extern int TERN_ee_wr (int a, uchar d);

extern UINT __TERN_fb_ad16 (uchar k);
extern UINT TERN_fb_ad16 (uchar k);

// ---------------------------------------------------------------------------
// Disk Routines

//#include <filegeo.h>

#define FNLEN  11        // 8.3 filename length, 8+3, '.' is assumed.

// File Operation: ERROR CODES

#define fOK           0  // No errors reported.
#define fEND          1  // End of directory reached.
#define fERR          2  // Error encountered working with file.
#define fDISKFULL     3  // Disk full.
#define fWRITE_ERROR  4  // A low-level write operation failed.
#define fREAD_ERROR   5  // A low-level Read operation failed.
#define fCORRUPTED    6  // An empty cluster was found in FAT chain.
#define fDIRFULL      7  // Directory entries on this disk.
#define fINVALID      8  // An invalid call to findfirst or findnext.
#define fEOF          9  // End of file reached while reading.
#define fILLEGAL   0x0A  // Tried to read a "w" file or write a "r" file.
#define fMEMORY    0x0B  // An operation failed to secure enough memory.

// These constants are used by StampTimeHMSMDY()

#define TDCREATED   'c'  // 'create' date and time.
#define TDMODIFIED  'm'  // 'modified' date and time.
#define TDACCESSED  'a'  // 'accessed' date.

// File attribute definitions.  Stored in fs_descrip.ff_attrib.

#define FA_ARCH    0x20  // Archive bit.
#define FA_HIDDEN  0x02  // Hidden file bit, this code displays hidden files.
#define FA_SYSTEM  0x04
#define FA_RDONLY  0x01  // Read Only bit, file cannot be written or appended.
#define FA_CLEAR   0x0   // No attributes are set.
#define FA_LABEL   0x08  // Volume label.
#define FA_DIREC   0x10  // Directory.
#define FA_EXTEND  0x0f  // Extended/LFN attributes, ignored by this code

#define FS_NO_DMA0 0x01  // do not use DMA0 for file system
#define FS_ONE_FAT 0x02  // keep only one copy of the FAT

#define O_READ     0x1
#define O_WRITE    0x2
#define O_APPEND   0x4

struct fs_descrip {
    UINT ff_dirpos;      // The number of the directory entry for this file
    UINT ff_start;       // The starting cluster
    UINT ff_current;     // The cluster currently being written to
    char ff_attrib;      // Attribute byte, see FA_xxx above.
    char ff_mode;        // Either fREAD or fWRITE or fCLOSED.
    UINT ff_ctime;       // File creation time
    UINT ff_cdate;       // File creation date
    UINT ff_mtime;       // File modified time
    UINT ff_mdate;       // File modified date
    UINT ff_adata;       // File accessed date (no time stored)
    LONG ff_fsize;       // File size in bytes
    LONG ff_position;    // the 'read' pointer
    int  ff_status;      // result code
    char ff_name[FNLEN+1];  // 8.3 file name
    uchar * ff_buf;       // Cluster buffer, sectors must be read and written
    uchar ff_touched;     // flag that indicates current buffer changed
    UINT ff_DirCluster;  // Directory cluster for this file
};

#ifdef FOO
extern int  TERN_fs_initPCFlash (void);
extern UINT TERN_fs_availableSpace (void);
extern int  TERN_fs_setFlags (UINT f);
extern FS * TERN_fs_fopen (const char * fn, int mode);
extern int  TERN_fs_change_dir (const char * dir);
extern int  TERN_fs_create_dir (const char * dir);
extern int  TERN_fs_rm_dir (const char * dir);
extern int  TERN_fs_get_dir (const char * dir, FS * fs);
extern int  TERN_fs_findfirst (char * path, FS * fs);
extern int  TERN_fs_findnext (FS * fs);
extern uchar TERN_fs_fgetc (FS * fs);
extern uchar TERN_fs_fputc (const uchar s, FS * fs);
extern char TERN_fs_fgets (char * s, int n, FS * fs);
extern int  TERN_fs_fclose (FS * fs);
extern int  TERN_fs_fseek (FS * fs, LONG index);
extern int  TERN_fs_fremove (FS * fs);
extern LONG TERN_fs_frdump (FS * fs, uchar far * dst, LONG length);
extern LONG TERN_fs_fwdump (FS * fs, uchar far * src, LONG length);
extern uchar TERN_fs_callocate (FS * fs, UINT clusters);
extern FS * TERN_fs_fopen_callocate (const char * fn, UINT clusters);
extern void TERN_fs_rename (FS * fs, char * fn);
extern char TERN_fs_fflush (FS * fs);
#endif


extern void TERN_fs_StampTimeHMSMDY (FS * fs, char TDtype,
    UINT hh, UINT mm, UINT ss, UINT mo, UINT dy, UINT yr);

#if 0 // need va_* stuff - skip for now, since we don't use it
extern int  TERN_fs_fprintf (FS * fs, const char * format, ...);
#endif

// ---------------------------------------------------------------------------
// serial port routines from the tern. we rewrite the functions so
// that the COM structure of the original tern routines is implied.

extern void TERN_s0_init (uchar b, uchar *ibuf, int isiz, uchar * obuf, int osiz);
extern int  TERN_putser0 (uchar outch);
extern int  TERN_putsers0 (char * str);
extern int  TERN_puts0 (uchar * str, uchar n);
extern uchar TERN_s0_cts (void);
extern void TERN_s0_rts (char b);
extern uchar TERN_getser0 (void);
extern int  TERN_getsers0 (int len, char * str);
extern void TERN_s0_close (void);
extern uchar TERN_serhit0 (void);
extern void TERN_clean_ser0 (void);
extern void TERN_s0_echo (char onoff);
extern void TERN_s0_flush (void);
extern int  TERN_s0_used (void);

extern void TERN_s1_init (uchar b, uchar *ibuf, int isiz, uchar *obuf, int osiz);
extern int  TERN_putser1 (uchar outch);
extern int  TERN_putsers1 (char * str);
extern int  TERN_puts1 (uchar * str, uchar n);
extern uchar TERN_s1_cts (void);
extern void TERN_s1_rts (char b);
extern uchar TERN_getser1 (void);
extern int  TERN_getsers1 (int len, char *str);
extern void TERN_s1_close (void);
extern uchar TERN_serhit1 (void);
extern void TERN_clean_ser1 (void);
extern void TERN_s1_echo (char onoff);
extern void TERN_s1_flush (void);
extern int  TERN_s1_used (void);

extern LONG TERN_ticks2msec (LONG t);
extern LONG TERN_msec2ticks (LONG m);

#if USE_HOKEY_VOFF
extern int __TERN_voff_arm_1s (void);
extern int __TERN_voff_arm (void);
extern int __TERN_voff_off (void);
extern int __TERN_voff_cfg (uchar h, uchar m);
extern int TERN_voff_arm (void);
extern int TERN_voff_off (void);
extern int TERN_voff_cfg (uchar h, uchar m);
// defined in nettask.c
extern int VOFF_MAYBE_SHUTDOWN (void);
#endif

/* since Paradigm's sprintf, sscanf, malloc, and free are not reentrant,
here are our semaphore-wrapped versions. the functions share the semaphore
with the other library functions just to be safe. if the function would
have returned a error, xprintf returns zero and places the error code in
its argument, x.
*/

extern int TERN_sscanf (const char * p, const char * f, ...);
extern int TERN_sprintf (char * p, const char * f, ...);
extern int TERN_xprintf (int * x, char * p, const char * f, ...);

extern void * TERN_malloc (int size);
extern void TERN_free (void * ptr);

// ---------------------------------------------------------------------
// FreeRTOS wrappers, etc



#define TERN_portEXIT_CRITICAL()       __asm{ popf }
#define TERN_portENTER_CRITICAL()      __asm{ pushf } \
                                       __asm{ cli   }
#define TERN_portDISABLE_INTERRUPTS()  __asm{ cli }
#define TERN_portENABLE_INTERRUPTS()   __asm{ sti }
#define TERN_portNOP()                 __asm{ nop }
#define TERN_portYIELD()               __asm{ int 0x80 }

extern void TERN_vTaskStartScheduler (void);
extern ULNG TERN_xTaskGetTickCount (void);
extern void TERN_xTaskResumeAll (void);
extern void TERN_vTaskSuspendAll (void);
extern void TERN_vTaskResume (void * TaskToResume);
extern void TERN_vTaskSuspend (void * TaskToSuspend);
extern void TERN_vTaskDelay (ULNG TicksToDelay);

extern void TERN_vQueueDelete (void * q);
extern void * TERN_xQueueCreate (UINT count,  UINT size);
extern void * TERN_SemaphoreCreateBinary (void);
extern void * TERN_xSemaphoreCreateMutex (void);
extern int TERN_xSemaphoreTake (void * q, ULNG t);
extern int TERN_xSemaphoreGive (void * q);
extern int TERN_xQueueSend (void * q, const void * item, ULNG t);
extern int TERN_xQueueReceive (void * q, void * item, ULNG t);
extern int TERN_uxQueueMessagesWaiting (void * q);

typedef void (* task_t)(void *);
#define CSTR const char * const

extern int TERN_xTaskCreate (task_t function, CSTR name, UINT stackdepth,
                             void * parameters, UINT priority, void * created);

extern void TERN_vTaskDelete (void * TaskToDelete);
extern void * TERN_xTaskGetCurrentTaskHandle (void);

#undef CSTR
//#undef uchar
//#undef UINT
//#undef LONG
//#undef ULNG
//#undef FS

#endif
