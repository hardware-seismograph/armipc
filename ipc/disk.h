/* DISK.H - disk subsystem

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary 
work and also under the United States Copyright Act of 1976, as amended. 
Distribution and access are limited only to authorized persons. Disclosure, 
reproduction, distribution or use, such as use to prepare derivative works, 
may violate federal andn state laws.
*/

/* HISTORY
10-20-08 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifndef __DISK_H__
#define __DISK_H__

/* file number notes

Rather than file names, the disk subsystem references files by file number.
A file number that is in the range 0x0000 to 0x7EEF is considered an absolute
reference to a data file. Special files such as settings files, log files,
etc. have file numbers in the range 0x7FF0 to 0x7FFF. 

A file number that is negative is a relative reference to a data file starting
from the last data file (largest file number) stored. For example, file number
-1 is the last data file stored on disk. Negative file numbers never refer to 
special files.

The disk_files and disk_delete functions accept two file number arguments to
specify a range of files. To specify a single file for these commands, use the
same file number for both arguments.

The disk_store function accepts the special file, DS_NEXTFILE, which directs
the function to store the file as the next available file number. In any other
context DS_NEXTFILE has no special meaning. 
*/

/* identifiers for special files */

#define DS_NETCFG_Q  0x7FF9  // network (tern) configuration, requested
#define DS_NETCFG_A  0x7FF8  // network (tern) configuration, actual
#define DS_DEVCFG_Q  0x7FF7  // device configuration (factory setup) requested
#define DS_DEVCFG_A  0x7FF6  // device configuration (factory setup) actual
#define DS_DEVSET_Q  0x7FF5  // device settings, requested
#define DS_DEVSET_A  0x7FF4  // device settings, actual
#define DS_MESSAGES  0x7FF3  // reference to the message log file 
#define DS_DEBUGLOG  0x7FF2  // reference to the log file used for debugging
#define DS_GENERAL   0x7FF1  // file for general purpose use
#define DS_NEXTFILE  0x7FF0  // special designator for disk_store()

/* result codes,  disk errors */
 
#define DS_OK             0
#define DS_FAULT         -1
#define DS_MEDIAERROR    -2
#define DS_FILENOTFOUND  -3
#define DS_PARTIALREAD   -4
#define DS_READERROR     -5
#define DS_WRITEERROR    -6
#define DS_FILEEXISTS    -7
#define DS_DISKFULL      -8
#define DS_INTEGRITY     -9
#define DS_BUSY          -10

/* directory item */

typedef struct {
    unsigned short file;
    unsigned short size; 
    unsigned char  yr;
    unsigned char  mo;
    unsigned char  dy;
    unsigned char  hh;
    unsigned char  mm;
    unsigned char  ss;
    unsigned short junk;
    unsigned char  type;
    unsigned char  opts;
    unsigned char  part;
    unsigned char  totl;
} disk_diritem;

/* interface functions */

/* int disk_intialize (void) - called once to initialize the disk subsytem.
returns DS_* result code. 
*/

extern int disk_initialize (void);

/* int disk_shutdown (void) - called once to completely shutdown the disk
subsystem, free memory, etc. returns DS_* result code.
*/

extern int disk_shutdown (void);

/* int disk_safe_suspend (void * t) - suspends the task given by the
task handle, t, or the executing task if t is NULL. The function waits for
disk i/o to complete before suspending the task. Returns DS_OK for success
or DS_BUSY if the task could not be suspended.
*/

extern int disk_safe_suspend (void * t);

/* int disk_mediadetect (void) - performs a basic check to ensure that
a memory card is present (and functional). returns a DS_* result code.
*/

extern int disk_mediadetect (void);

/* int disk_store (int fn, int n, unsigned char * s) - writes n bytes from
pointer s to the file given by the file number, fn (see file number notes).
returns DS_* result code.
*/

extern int disk_store (int fn, int n, unsigned char * s);

/* int disk_store_insitu (int fn, unsigned char * s, int n) - writes n bytes
from pointer s to the file given by the file number, fn (see file number 
notes). the data are directly written into file, so the file must be comprised
of a contiguous set of sectors. returns DS_* result code.
*/

extern int disk_store_insitu (int fn, int n, unsigned char * s);

/* int disk_fetch (unsigned char * s, int n, int fn) - reads up to n bytes 
into memory pointer to by s from the file number, fn (see file number notes).
returns DS_* result code on error or the number of bytes read.
*/

extern int disk_fetch (int fn, int n, unsigned char * s);

/* int disk_fetch_default (unsigned char * s, int n, int fn) - same as
disk_fetch except that the function reads the default (.ini) version of 
the file number.
*/

extern int disk_fetch_default (int fn, int n, unsigned char * s);

/* int disk_append (unsigned char * s, int n, int fn) - writes n bytes from
pointer s to the end of the file given by the file number, fn (see file number
notes). returns DS_* result code.
*/

extern int disk_append (int fn, int n, unsigned char * s);

/* int disk_delete (int fn, int f2) - reads the files from the disk designated
by the file number range in fn and f2 (see file number notes). returns the
number of files deleted, or returns a DS_* result code if an error occurs.
*/

extern int disk_delete (int fn, int f2);

/* int disk_files (int fn, int f2, int nd, disk_diritem * dp) - reads the disk
directory and creates an array of up to nd number of disk_diritem objects.
returns the number of disk_diritem objects created, or returns a DS_* result
code if an error occurs. the listing is controlled by the file number range 
designated in fn and f2 (see file number notes).
*/

extern int disk_files (int fn, int f2, int nd, disk_diritem * dp);

/* int disk_fsize (void) - returns the file size in bytes of the file number
given in fn (see file number notes), or returns a DS_* result code if an 
error occurs.
*/

extern int disk_fsize (int fn);

/* int disk_count (void) - returns the number of data files (files excluding
special files) on the disk or returns a DS_* result code if an error occurs.
*/

extern int disk_count (void);

/* int disk_next (void) - returns the "next" file number (the last file
number used + 1) or a DS_* result code if an error occurs. fast binary
search for the next file number, but is not guaranteed to work correctly
if there are missing files.
*/

extern int disk_next (void);

/* utility functions to map file numbers to absolute file numbers.
function disk_parse2 ensures that fn <= f2 and that the ranges do cross
the boundary between data files (g3k) and special files (dat). both
functions return a DS_* result code.
*/

extern int disk_parse1 (int * fn);
extern int disk_parse2 (int * fn, int * f2);

#endif
