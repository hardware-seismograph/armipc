/* VERSION.H - newlife project

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary
work and also under the United States Copyright Act of 1976, as amended.
Distribution and access are limited only to authorized persons. Disclosure,
reproduction, distribution or use, such as use to prepare derivative works,
may violate federal andn state laws.
*/

/* HISTORY
12-01-09 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/
/*


// **********************************************************************************************
// Version: 415 ***bt - 01/02/2018
// Changes are to bridge.c and nettsk.c
// 1. In bridge.c - Added logging to the cmd_put() function to log record functions to the F1
// file.
// 2. In nettask.c - In the Raven code, added a check for network response of "VzW" and if the
// network is not recognized then force a default to RVERIZON.
// **********************************************************************************************

// **********************************************************************************************
// Version: 414 ***bt - 09/06/2017
// All changes are to nettask.c
// 1. In the modem_powerdn() function, took out the 120 second wait.  Look for 09/06/2017.

// **********************************************************************************************

// **********************************************************************************************
// Version: 413 ***bt - 02/01/2017
// All changes are to nettask.c
// 1. Added support for SMS messaging for the Raven (RV-50 AirPort) modem.

// **********************************************************************************************

// **********************************************************************************************
// Version: 412 ***bt - 11/02/2016
// All changes are to nettask.c
// 1. Added support for a pure ethernet connection for the Cloudgate modem.   You must use the
//    proper SDK on the Cloudgate modem.
//    Added a new modem type kCFG_ETHERNET and a new function ethCheckRecord() that performs the
//    records/alarm check but does not do SMS checking.

// **********************************************************************************************
// Version: 411 ***bt - 06/09/2016
// All changes are to nettask.c
// 1. Added support for telcel carrier (Mexico).
// 2. Changed the T-Mobile APN string from internet2.voicestream.com to fast.t-mobile.com.

// **********************************************************************************************
// Version: 410 ***bt - 03/29/2016
// All changes are to nettask.c
// 1. Added support for Sierra Wireless Raven Modem Model: (RV50).
// 2. Added a new function qos_Raven().   Converts the QOS values returned from the Raven.
// 3. Added sms_Raven_modem().  Note: the Raven RV50 does not support SMS messaging.
// 4. Changed the function net_deduce_carrier().   Send ATI0 and see if RV50 is returned.
// 5. Modified the shutdown for the CE910, DE910 and the HE910.  These are all in the
//    modem_powerdn() function.

// **********************************************************************************************
// Version: 409 ***bt - 10/07/2015
// All changes are to nettask.c
// Added a 1500ms delay between the ISP configure string and the dial string.  Look for 10/07/2015

// **********************************************************************************************
// Version: 408 ***bt -	07/27/2015
// All changes are the nettask.c
// 1. Added support for the VIVO ISP (Brazil).
// 2. Removed the 2 5 second waits after the CFUN modem command.
// 3. Removed a number of smart_bridge_SetREQ();
// 4. Removed TERN_vTaskDelay(u); from the end of the netask loop.

// **********************************************************************************************
// Version: 407 ***bt -	07/01/2015
// 1. Send the AT+CREG? command with a 1 second wait between each try.  The Tern will continue
// until an ERROR, (bad command), +CREG: 0,1 (registered) or +CREG: 0,5 (roaming).  Actually we
// just look for 0,1 and 0,5 due to some modems not putting a space after +CREG:

// **********************************************************************************************
// Version: 406 ***bt -	06/24/2015
// 1. Fixed the check cycle (wakeup) in Rev. 2 mode.  It was not waking up and doind the SMS checks.
// 2. Fixed the problem where if an event occurs during the time that the nettask is getting ready
// to sleep for the check cycle.  (Fixes are in nettask.c).

// **********************************************************************************************
// Version: 405 ***bt -	06/16/2015

Tern/IPC
1.  An event can now interrupt the SMS checking cycle.  If an event (trigger or alarm) occurs during the
time that time that the IPC is checking for SMS messages from the modem.  The check will immediately be
halted and a heartbeat message or record will be sent.
2. Improved the handling of record numbers to assure that we do not send duplicate messages.  This also
insures that we don't miss an heatbeat messages.
3.  The operation in the Rev2 mode (modem always on) was corrected to prevent the modem from locking up.

Seismograph
1.  Changed the method of sending alarms alerts so that it will send the same alarm no matter what mode the
seismograph is in.
2.  Put in a delay after DTR is asserted to the MAXON-RS232 chip.  This is to assure that DTR toggles the
port properly.
*/
// ***********************************************************************************************
// Version: 404 ***bt 06/05/2015
// This release fixes the problem that certain units have.  A timing problem that causes a multiple
// records to be sent.  gVAR.NN is set to 0 if gVAR.NE and gVAR.NL are equal.
// ***********************************************************************************************
// Version: 403 ***bt - 05/29/2015
// 1.  The previous version of the IPC would always check for SMS messages 10 times on powerup.  There
// is a 4 second delay between each check.  That means that if a heartbeat message was waiting to be
// sent it would delayed by at least 40 seconds. The previous version always sent an SMS clear to the
// modem.
//
// When the IPC wakes up after a power down sequence it checks two AV alarm flags:
//
// #define AV_NETCNTLIMIT	0x00040000		// Event alarm exceeded set limit
// #define AV_RECEXTALARM	0x00001000		// External alarm is active
//
// Before the IPC requests SMS message if either of these flags are set to 1, the IPC will not ask for
// any SMS messages.  It also will not send an SMS message clear command to the modem.  This will
// increase the speed of the heartbeat delivery by at least 40 seconds.  The returned command is set
// to force the IPC to call the server.
//
// The the case that neither of the alarm flags are set to 1, the IPC will ask for any SMS messages:
//	1.  It requests SMS message from the modem 10 times with a 4 second delay between requests.
//	    If no SMS messages are queued for delivery then the return command is do not dial the
//	    server.
//	2.  Between each SMS request to the modem there is a 4 second delay.  During this delay
//	    every 500ms the IPC checks for the event count and if it exceeded the AV_NETCNTLIMIT
//	    flag will be set to 1.
//	3.  During this time it also checks to see if an AV_ALARMOFF or an AV_ALARMOFF code has
//	    been received.  If the AV_NETCNTLIMIT and the AV_RECEXTALARM flags will both be set.
//	    Then the return code will be to call the server.
//	4.  If the IPC sends 10 SMS request messages with no message received the return code will
//	    not dial at this time.
//
// ************************************************************************************************
// Version: 402 ***bt - 05/14/2015
// 1. Added the waitForModem(void) function that sends AT on startup until the modem sends OK.
// 2. Added the modem_rev2dn() function to power off the modem when in Type 2 mode.
// 3. Changed the sms_gsm_modems(void) function to look at the alarm and external alarm flag.
//    If the flag is set the tern will not look for SMS messages and just pretend that it received
//    an SMS message so it will immediately dial out on an event.  It also looks for the alarm
//    cleared flag and will reset the alarm and external alarm flag.
// *************************************************************************************************

// Version: 401 ***bt -	07/01/2014
// This version is built with my lwip and rtos libraries.

// Version: 400 ***bt 06/17/2014
// Initial release of code that I built.

#ifndef __VERSION_H__
#define __VERSION_H__

#define kVERSION 100
#define kSERIAL 1234

#ifndef TERN_FB
#define TERN_FB  1
#endif

// use tern's Voff circuit? 0:NO 1:YES
#define USE_HOKEY_VOFF  1
#define USE_SLEEP_MODE  1

// include gps information in the heartbeat
#define INCLUDE_GPS  1

// disable modem during low power warning
#define LOW_POWER_DISABLES_MODEM  1


// memory configuration (see demo.cfg)
#define RAM  512
#define ROM  512

// maximum log file size (see bridge.c)
#define kMAXLOG  4096


// ---------------------------------------------------------------------------

#if ((TERN_EL + TERN_FB) != 1) || ((TERN_EL * TERN_FB) != 0)
#error "only one of TERN_FB or TERN_EL must be set to 1"
#endif

#if TERN_EL
#ifndef TERN_20Mhz
#ifndef TERN_40Mhz
#define TERN_40Mhz
#endif
#endif
#endif

#if TERN_FB
#ifndef TERN_20Mhz
#ifndef TERN_40Mhz
#define TERN_20Mhz
#endif
#endif
#endif

#define JIFFIES_PER_SECOND  1000

#endif
