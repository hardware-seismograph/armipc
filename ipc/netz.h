/* NET.H - basic tcp interface for lwip

Copyright (c) 2008 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary 
work and also under the United States Copyright Act of 1976, as amended. 
Distribution and access are limited only to authorized persons. Disclosure, 
reproduction, distribution or use, such as use to prepare derivative works, 
may violate federal andn state laws.
*/

/* HISTORY
11-02-08 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifndef __NET_H__
#define __NET_H__

#include "../lwip/include/ipv4/lwip/ip_addr.h"

#ifndef kSUCCESS
#define kSUCCESS  0
#endif

#ifndef kFAILURE
#define kFAILURE -1
#endif

#ifndef u16_t
#define u16_t  unsigned short
#endif

typedef struct {
    struct netconn * s;
    struct netbuf * qp;
    int             qo;
} mysocket, * SOCKET;

extern SOCKET net_socket (void);
extern SOCKET net_accept (SOCKET s);
extern int net_delete (SOCKET s);
extern int net_timeout (SOCKET s, int timeout);
extern int net_bind (SOCKET s, struct ip_addr * addr, u16_t port);
extern int net_connect (SOCKET s, struct ip_addr * addr, u16_t port);
extern int net_getaddr (SOCKET s, struct ip_addr * addr, u16_t * port, int lo);
extern int net_close (SOCKET s);
extern int net_listen (SOCKET s);
extern int net_get (SOCKET s, char * m, int n);
extern int net_gets (SOCKET s, char * m, int n);
extern void net_put (SOCKET s, const char * m, int n);
extern void net_puts (SOCKET s, const char * m);
extern int net_WaitHack (SOCKET s, int t);

// ------------------------------------------------------------------------

extern unsigned char * net_share_init (int size);
extern unsigned char * net_share_area (void);
extern int net_share_size (void);
extern int net_share_quit (void);

#endif
