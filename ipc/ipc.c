/* MODULE Application */

#include "../rtos/include/FreeRTOS.h"
#include "../rtos/include/task.h"
#include "board.h"
#include "ipc.h"


void lUDPLoggingPrintf(const char *apFmt, ...)
{
  return;
}


// Sets a user timer, duration is in ms
void setTimer(TIMER *timer, ulong duration)
{
	timer->start = clock();
	timer->duration = duration;
	timer->valid = (duration)? TRUE : FALSE;
	return;
}

// Checks the timer and returns (TRUE) if the timer has expired
int checkTimer(TIMER *timer)
{
 int result;

	if(timer->valid != TRUE)
		result = TRUE;
	else if((clock() - timer->start) > timer->duration)
	{
		timer->valid = FALSE;
		result = TRUE;
	}else
		result = FALSE;
	return(result);
}



#ifdef FFFFFFOOOOOOOOOOOOOOO
static portTASK_FUNCTION(network_task2, pvParameters)
{
 float f1, f2;
  (void)pvParameters; /* parameter not used */
  for(;;)
  {
    LED3_TOGGLE();
    vTaskDelay(100 / portTICK_RATE_MS);
  }
}

static portTASK_FUNCTION(bridge_task2, pvParameters)
{
  (void)pvParameters; /* parameter not used */
  for(;;)
  {
    LED2_TOGGLE();
    vTaskDelay(50 / portTICK_RATE_MS);
  }
}
#endif

ushort gSERIAL = kSERIAL;
ushort gVERSION = kVERSION;


// important! put this into our nvram space
static struct {
    volatile unsigned char oops;
    volatile unsigned char mode;
    volatile unsigned char flag;
    volatile unsigned char ___x;
    volatile int sleep;
    char note [124];
}gSTATE;

// ---------------------------------------------------------------------

void assert (const char * message)
{
    vTaskSuspendAll ();
    strncpy (gSTATE.note, message, 123);
    gSTATE.note[123] = 0;
    gSTATE.oops = 9;
//    ae_reset ();
    xTaskResumeAll ();
}

void lwip_assert (const char * message)
{
    assert (message);
}

// ---------------------------------------------------------------------

static int __debug (const char * note)
{
    char s [160];
    struct tm tm;

   RTC_read(&tm);

    sprintf(s, ". %02d/%02d/%02d %02d:%02d:%02dZ %1.138s\n",
             tm.tm_mon,
             tm.tm_mday,
             tm.tm_year,
             tm.tm_hour,
             tm.tm_min,
             tm.tm_sec,
             note);

    s[159] = 0;

//D!    disk_append (DS_DEBUGLOG, strlen(s), (unsigned char *) s);
    return 0;
}

// ---------------------------------------------------------------------
int voff_arm(void)
{
  return(1);
}

void voff_off(void)
{
  return;
}

void voff_arm_1s(void)
{
  return;
}

void sleep(int ms)
{
  return;
}

void pio_wr(char bit, char state)
{
  return;
}

int pio_rd(char bit)
{
  return(1);
}

void led(int onOff)
{
  return;
}

int ee_rd(int address)
{
  return(1234);
}

static int log_oops__ (void)
{
    gSTATE.note[123] = 0;
    switch (gSTATE.oops) {
    case 0x0: return 0;
    case 0x1: return __debug ("OOPS1 divide error");
    case 0x2: return __debug ("OOPS2 trace");
    case 0x3: return __debug ("OOPS3 non-maskable");
    case 0x4: return __debug ("OOPS4 breakpoint");
    case 0x5: return __debug ("OOPS5 INT0 overflow");
    case 0x6: return __debug ("OOPS6 array bounds");
    case 0x7: return __debug ("OOPS7 unused exception");
    case 0x8: return __debug ("OOPS8 esc opcode");
    case 0x9: return __debug (gSTATE.note);
    default:  return __debug ("OOPS? bad value");
    }
}

static void log_oops (void) {
    log_oops__ ();
}

static void clr_oops (void) {
    gSTATE.oops = 0;
}



// ---------------------------------------------------------------------

static void onoff_init (void)
{
    // initialize the io that's used with the pic on-off-plus controller

#define IO_WRN  0x0200  /* P25 (input, low) low battery warning */
#define IO_ST0  0x4000  /* P30 (input, low) onoff status {1} */
#define IO_ST1  0x0100  /* P24 (input, low) onoff status {1} */

    // {1} onoff status [ST1 ST0]
    // [1 1] running
    // [1 0] shutdown request
    // [0 1] short button press
    // [0 0] long button press

#if NOT_USED
    pio_wr (10, 0);    // RDY (output, high) - reset Voff override,
    pio_init (10, 2);  // and signal that other outputs are valid
    pio_wr (10, 0);

    pio_wr (13, 0);    // RST (output, high) - reset logger, removes
    pio_init (13, 2);  // power from the logger when asserted
    pio_wr (13, 0);

    pio_wr (14, 0);    // MC0 (output, high) - modem on control
    pio_wr (15, 0);    // MC1 (output, high) - modem off control
    pio_init (14, 2);
    pio_init (15, 2);
    pio_wr (14, 0);
    pio_wr (15, 0);

    pio_wr (31, 0);   // OFF (output, high) - shutdown control
    pio_init (31, 2);
    pio_wr (31, 0);


    // not sure if this is necessary
    pio_init (24, 2);
    pio_init (30, 2);
    pio_wr (24, 1);
    pio_wr (30, 1);
    pio_init (24, 1);
    pio_init (30, 1);
#endif
}

static void onoff_enable_io (void)
{
    pio_wr (10, 1);
}

#define dly0s 1200
#define dly1s 2000
#define dly2s 4000

static void onoff_reset_status (void)
{
    pio_wr (10, 1);
    sleep(dly0s);
    pio_wr (10, 0);
    sleep(dly1s);
}

static void onoff_shutdown (void)
{
    pio_wr (10, 1);
    sleep(dly0s);
    pio_wr (31, 1);
    sleep(dly1s);
    sleep(dly1s);
}

#define kONOFF_MASK  3
#define kONOFF_LONG  3
#define kONOFF_SHORT 2
#define kONOFF_OFF   1
#define kONOFF_RUN   0

static int __st (void)
{
    int a, b, c, d;
    a = 0;
    b = 0;
    c = 16;
    while (c-- > 0) {
        d = pio_rd(1);
        if (d & IO_ST1) a++;
        if (d & IO_ST0) b++;
        sleep(2);
    }
    d = 0;
    if (a > 7) d |= IO_ST1;
    if (b > 7) d |= IO_ST0;
    return d;
}

static int onoff_status (void)
{
#define __ST_MSK (IO_ST1|IO_ST0)
    switch (__st() & __ST_MSK) {
    case 0:      return kONOFF_LONG;
    case IO_ST0: return kONOFF_SHORT;
    case IO_ST1: return kONOFF_OFF;
    }
    return kONOFF_RUN;
}

static void state_init (void)
{
    gSTATE.flag = 0;

    // on-off button status, etc.
    gSTATE.flag |= onoff_status ();

    // initialize serio flag
    if (!getCTS(BRIDGE_PORT))
        gSTATE.flag |= kFLAG_SERIO;

    // initialize modem flag
    // if (??)
    //     gSTATE.flag |= kFLAG_MODEM;

    switch (gSTATE.flag & kONOFF_MASK) {
    case kONOFF_SHORT:
    case kONOFF_LONG:
        gSTATE.mode = kMODE_POWER;
        gSTATE.___x = kMODE_POWER;
        break;
    case kONOFF_OFF:
        gSTATE.mode = kMODE_WAKEN;
        break;
    }
}

// ---------------------------------------------------------------------

#define kFOREVER  ((long)180L * JIFFIES_PER_SECOND)

static void * __supervisor = NULL;

int supervisor_suspend_p (void)
{
    return gSTATE.flag & kSUSPEND_ALL;
}

void supervisor_SLP (int target)
{
    if (xSemaphoreTake (__supervisor, kFOREVER) == pdTRUE) {
        gSTATE.sleep = (target > 0) ? target : 1;
        xSemaphoreGive (__supervisor);
    }
}

void supervisor_SET (unsigned char flag)
{
    if (xSemaphoreTake (__supervisor, kFOREVER) == pdTRUE) {
        gSTATE.flag |= flag;
        xSemaphoreGive (__supervisor);
    }
}

void supervisor_CLR (unsigned char flag)
{
    if (xSemaphoreTake (__supervisor, kFOREVER) == pdTRUE) {
        gSTATE.flag &= ~flag;
        xSemaphoreGive (__supervisor);
    }
}

void supervisor_END (unsigned char mode)
{
    int i = (int) (kFOREVER / 10);

    if (xSemaphoreTake (__supervisor, kFOREVER) == pdTRUE) {
        gSTATE.flag |= kSUSPEND_ALL;
        xSemaphoreGive (__supervisor);
        while (gSTATE.flag & (kBRIDGE_BUSY | kNETWRK_BUSY)) {
            vTaskDelay (10);
            if (i-- > 0) continue;
            assert ("supervisor_END: unable to suspend_all");
        }
        vTaskSuspendAll ();
        switch (mode) {
        case kMODE_RESET:
            bridge_logadd (MESSAGE_RESET, NULL);
        case kMODE_ERROR:
            gSTATE.mode = mode;
            //ae_reset ();
            assert ("supervisor_END: TERN_ae_reset failed");
            break;
        case kMODE_TIMER:
            bridge_logadd (MESSAGE_TIMER, NULL);
        case kMODE_FAULT:
        case kMODE_SLEEP:
#ifdef USE_HOKEY_VOFF
            gSTATE.mode = mode;
            onoff_reset_status ();
            voff_arm ();
            voff_off ();
            voff_arm_1s ();
            sleep(dly1s);
            onoff_enable_io ();
            // prepare for an unplanned shutdown
            bridge_logadd (MESSAGE_EXTRA, "IF: TERN_voff_off failed");
            gSTATE.mode = kMODE_ERROR + kMODE_READY; // we're up
            gSTATE.___x = 0;
            break;
#endif
        case kMODE_POWER:
            bridge_logadd (MESSAGE_PWRDN, NULL);
            gSTATE.mode = mode;
            gSTATE.___x = mode;
            onoff_shutdown ();
            assert ("supervisor_END: onoff_shutdown failed");
            break;
        default:
            assert ("supervisor_END: bad mode");
        }
        gSTATE.flag &= ~kSUSPEND_ALL;
        xTaskResumeAll ();
    }
    else {
        assert ("supervisor_END: can't lock");
    }
}

void supervisory (void * parameter)
{
    do {
 
      while (onoff_status () != kONOFF_OFF)
    {
        LED2_TOGGLE();
        vTaskDelay(50 / portTICK_RATE_MS);
    };
      // put the recorder to sleep
        supervisor_SET (kBRIDGE_PEND);
        bridge_SetREQ (0x47);
        while (gSTATE.flag & kBRIDGE_PEND) {
            vTaskDelay (6);
        }
        supervisor_CLR (kBRIDGE_PEND);
        sleep(dly1s);

        supervisor_END (kMODE_POWER);

        // should never get here
        assert ("supervisory: TERMINATED");

    } while (1);
    // should never get here
}

// ---------------------------------------------------------------------

static void button_long_press (void)
{
    int t;

    led (1);
    bridge_logadd (MESSAGE_FILES, NULL);
//D!    t = disk_fetch_default (DS_NETCFG_Q, net_share_size(), net_share_area());
//D!    if (t > 0) disk_store (DS_NETCFG_Q, t, net_share_area());
//D!    t = disk_fetch_default (DS_DEVSET_Q, net_share_size(), net_share_area());
//D    if (t > 0) disk_store (DS_DEVSET_Q, t, net_share_area());
    sleep(dly2s);
    led (0);
}

static void restore_initial_state (void)
{
    gSTATE.oops = 0;
    gSTATE.mode = kMODE_READY;

    network_nvram_init ();

    // whatever initial state means now (?!*)
}

static void restore_partial_state (void)
{
    // probably not possible to do this (?!*)
}

unsigned long network_av (void);

static void gstate_snapshot (char * s)
{
    sprintf (s,"IF: gSTATE oops: %02x  mode: %02x  flag: %02x  AV: %08lx",
             gSTATE.oops, gSTATE.mode, gSTATE.flag, network_av());
}

#include "fs/include/efs.h"

extern volatile void *pxCurrentTCB;

void hang(void)
{
  while(1)
    ;
}


void startIPC(void)
{

 EmbeddedFileSystem efs;
 EmbeddedFile filew, filer;
 unsigned short i, e;
 euint8 buf[512];

#ifdef FOOOOOOOOOO
///////  i = HW_PINCTRL_MUXSEL0_bit.BANK4_PIN10;
  if(efs_init(&efs, 0) !=0 ) 
  {
      hang();
  }

  if(file_fopen(&filer, &efs.myFs, "orig.txt", 'r') != 0)
  {
      hang();
  }

  if(file_fopen(&filew ,&efs.myFs, "copy.txt", 'w' ) != 0)
  {
      hang();
  }

  while(e = file_read(&filer, 512 , buf)) 
  {
      file_write(&filew, e, buf) ;
  }

  file_fclose(&filer);
  file_fclose(&filew);

//////  file_umount(&efs.myFs);
#endif

  
  gSERIAL = kSERIAL;

//    ae_init ();
//    patch_interrupt_table ();

    onoff_init ();
    state_init ();

    onoff_enable_io ();

#if USE_HOKEY_VOFF
    voff_arm_1s ();

    if(gSTATE.mode == kMODE_TIMER)
    {
        if(!voff_maybe_shutdown())
        {
            onoff_reset_status ();
            voff_arm ();
            voff_off ();
            voff_arm_1s ();
            sleep(dly1s);
            onoff_enable_io ();
        }
    }

#if USE_SLEEP_MODE
    if (gSTATE.mode == kMODE_SLEEP) {
        if (! (gSTATE.flag & kFLAG_SERIO)) {
/*            if (network_modem_always_on_p ())	      ***bt - 06/19/2015
                if (network_modem_test_wakeup ())
                    gSTATE.flag |= kFLAG_MODEM;	      ***bt - 06/19/2015 */
            if (! (gSTATE.flag & kFLAG_MODEM)) {
                if (gSTATE.sleep > 0) {
                    gSTATE.sleep--;
                    if (gSTATE.sleep > 0) {
                        onoff_reset_status ();
                        voff_arm ();
                        voff_off ();
                        voff_arm_1s ();
                        sleep(dly1s);
                        onoff_enable_io ();
                    }
                }
            }
        }
    }
#endif
#endif

//D!    disk_initialize ();
    log_oops ();

    bridge_init ();
    network_init ();

    switch (gSTATE.mode) {
    default:
    case kMODE_ERROR:
        bridge_logadd (MESSAGE_FAULT, NULL);
        gstate_snapshot (gSTATE.note);
        bridge_logadd (MESSAGE_EXTRA, gSTATE.note);
    case kMODE_RESET:
        restore_initial_state ();
        break;
    case kMODE_POWER:
        bridge_logadd (MESSAGE_PWRUP, NULL);
        if (gSTATE.mode != gSTATE.___x)
            bridge_logadd (MESSAGE_EXTRA, "IF: suspicious power_on");
        restore_initial_state ();
        if ((gSTATE.flag & kONOFF_MASK) == kONOFF_LONG)
            button_long_press ();
        break;
    case kMODE_TIMER:
        bridge_logadd (MESSAGE_TMRON, NULL);
    case kMODE_SLEEP:
    case kMODE_WAKEN:
        restore_partial_state ();
        break;
    case kMODE_FAULT:
        bridge_logadd (MESSAGE_EXTRA, "IF: recover from disk fault");
        restore_partial_state ();
        break;
    }

    clr_oops ();

    if (gSTATE.flag & kFLAG_MODEM) {
        network_modem_need_wakeup ();
    }

#define SLEEPING_P  ((void *) (gSTATE.mode == kMODE_SLEEP))

    __supervisor = xSemaphoreCreateMutex ();

/////////  sys_thread_new ("N", network_task, SLEEPING_P, 512, 2);

    if (xTaskCreate(
        network_task,  /* pointer to the task */
        (const portCHAR *)"Network", /* task name for kernel awareness debugging */
        configMINIMAL_STACK_SIZE, /* task stack size */
        (void*)NULL, /* optional task startup argument */
        tskIDLE_PRIORITY,  /* initial priority */
        (xTaskHandle *)NULL /* optional task handle to create */
      ) != pdPASS) {
    /*lint -e527 */
    for(;;){}; /* error! probably out of memory */
    /*lint +e527 */
  }
  
    
  if (xTaskCreate(
        bridge_task,  /* pointer to the task */
        (const portCHAR *)"Bridge", /* task name for kernel awareness debugging */
        configMINIMAL_STACK_SIZE, /* task stack size */
        (void*)NULL, /* optional task startup argument */
        tskIDLE_PRIORITY,  /* initial priority */
        (xTaskHandle *)NULL /* optional task handle to create */
      ) != pdPASS) {
    /*lint -e527 */
    for(;;){}; /* error! probably out of memory */
    /*lint +e527 */
  }

#ifdef FOOOOOO
  if (xTaskCreate(
        supervisory,  /* pointer to the task */
        (const portCHAR *)"SuperV", /* task name for kernel awareness debugging */
        configMINIMAL_STACK_SIZE, /* task stack size */
        (void*)NULL, /* optional task startup argument */
        tskIDLE_PRIORITY,  /* initial priority */
        (xTaskHandle *)NULL /* optional task handle to create */
      ) != pdPASS) {
    /*lint -e527 */
    for(;;){}; /* error! probably out of memory */
    /*lint +e527 */
  }
#endif
  
/////    xTaskCreate (bridge_task, "B", 512, NULL, 3, NULL);
/////    xTaskCreate (supervisory, "S", 512, NULL, 2, NULL);

    // prepare for an unplanned shutdown
    gSTATE.mode = kMODE_ERROR + kMODE_READY; // we're up
    gSTATE.___x = 0;

    vTaskStartScheduler ();

    assert("main: should never get here");

    for(;;);
}

/* END Application */
