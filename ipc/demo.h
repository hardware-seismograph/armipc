/* DEMO.H - newlife project

Copyright (c) 2011 by Aegean Associates, Inc.  All rights reserved.

This is a generally unpublished work which is protected both as a proprietary 
work and also under the United States Copyright Act of 1976, as amended. 
Distribution and access are limited only to authorized persons. Disclosure, 
reproduction, distribution or use, such as use to prepare derivative works, 
may violate federal andn state laws.
*/

/* HISTORY
03-28-11 Created - Demitrios Vassaras, Aegean Associates, Inc. (dv)
*/

#ifndef __DEMO_H__
#define __DEMO_H__

// demo.c

#define kFLAG_ST0     0x01
#define kFLAG_ST1     0x02
#define kFLAG_MODEM   0x04
#define kFLAG_SERIO   0x08

#define kBRIDGE_BUSY  0x10
#define kNETWRK_BUSY  0x20
#define kBRIDGE_PEND  0x40
#define kSUSPEND_ALL  0x80

#define kMODE_POWER  7
#define kMODE_RESET  1
#define kMODE_TIMER  2 
#define kMODE_SLEEP  3
#define kMODE_READY  4
#define kMODE_WAKEN  5
#define kMODE_FAULT  6
#define kMODE_ERROR  8

extern unsigned short gSERIAL;
extern unsigned short gVERSION;

extern int  supervisor_suspend_p (void);
extern void supervisor_SET (unsigned char flag);
extern void supervisor_END (unsigned char mode); 
extern void supervisor_CLR (unsigned char flag);
extern void supervisor_SLP (int minutes);
extern void assert (const char * text);

// nettask.c

extern int  network_init (void);
extern int  network_nvram_init (void);
extern void network_task (void * parameters);
extern void network_BumpNN (void);
extern void network_SetRecAV (unsigned short u);

extern int  network_modem_always_on_p (void);
extern int  network_modem_test_wakeup (void);
extern void network_modem_need_wakeup (void);

// bridge.c

#define MESSAGE_EXTRA  0x800000feL
#define MESSAGE_FILES  0x800000fdL
#define MESSAGE_PWRDN  0x800000fcL
#define MESSAGE_PWRUP  0x800000fbL
#define MESSAGE_TIMER  0x800000faL
#define MESSAGE_TMRON  0x800000f9L
#define MESSAGE_RESET  0x800000f8L
#define MESSAGE_FAULT  0x800000f7L
#define MESSAGE_LOPWR  0x800000f6L

extern int  bridge_init (void);
extern void bridge_task (void * parameters);
extern uint bridge_GetREQ (void);
extern void bridge_SetREQ (unsigned int c);
extern void bridge_logfix (int n, int sz, char * mp);
extern void bridge_logadd (ulong u, char * s); 
extern int  bridge_is_active (void);

#endif
