/** ###################################################################
**     Filename  : ipc.h
**     Project   : ARM Tern Replacement
**     Processor : I.mx28
**     Compiler  : IAR Systems Compiler
**     Date/Time : 
**     Contents  :
**         User source code
**
** ###################################################################*/

#ifndef __Application_H
#define __Application_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time32.h>
#include <iar_dlmalloc.h>

#define uint unsigned int
#define uchar unsigned char
#define ushort unsigned short
#define ulong unsigned long

#include <intrinsics.h>
#include "board.h"
#include "arm_comm.h"
#include "arm926ej_cp15_drv.h"
#include "ttbl.h"
#include "drv_icoll.h"

#include "version.h"
#include "serial/serial.h"
#include "api.h"
#include "ppp.h"
#include "tcp.h"
#include "disk.h"
#include "netz.h"
#include "demo.h"
#include "FreeRTOS.h"
#include "tcp.h"


#define VERSION_STRING "V00.01 (c) 2018 Geosonics Inc."

#ifdef FOO
typedef struct TM;
{
  int tm_sec;   // Sec
  int tm_min;   // Min
  int tm_hour;  // Time
  int tm_mday;  // Day
  int tm_mon;   // Month
  int tm_year;  // Year
  int tm_wday;  // Day of the week
  int tm_yday;  // Elapsed date Among from the start of the year
  int tm_isdst; // Season flag, such as daylight saving time
}TM;
#endif

/* MODULE Application */

extern unsigned long realMS;

// The elapsed timer structure
typedef struct TIMER
{
	int valid;
	ulong duration;
	ulong start;
}TIMER;

void setTimer(TIMER *timer, ulong duration);
int checkTimer(TIMER *timer);


void APP_Run(void);

int detectSSP0(void);
int detectSSP1(void);
int ee_wr(int address, uchar data);
int ee_rd(int address);
int rtc1_init(uchar * s);
void pio_init (char bit, char mode);
void pio_wr (char bit, char data);
int pio_rd (char bit);
void led (int on);
int rtc1_init (uchar * s);
int voff_arm (void);
uint fb_ad16(uchar k);
void delay_ms(int ms);
int voff_maybe_shutdown(void);
void led(int onOff);

void RTC_read(struct tm *tm);

int xprintf(int *x, char *p, const char  *f, ...);


/* END Application */

#endif

